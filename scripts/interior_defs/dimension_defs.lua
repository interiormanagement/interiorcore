local INTERIOR_WIDTHS = {
	10,
	12,
	16,
    15, -- TUNING.ROOM_TINY_WIDTH
	18, -- TUNING.ROOM_SMALL_WIDTH
    24, -- TUNING.ROOM_MEDIUM_WIDTH
    26, -- TUNING.ROOM_LARGE_WIDTH
}

local INTERIOR_HEIGHTS = {
	5,
	6,
	7,
	10, --TUNING.ROOM_TINY_DEPTH
	11,
	12, -- TUNING.ROOM_SMALL_DEPTH
	13,
    16, -- TUNING.ROOM_MEDIUM_DEPTH
    18, -- TUNING.ROOM_LARGE_DEPTH
}

-- TODO: Find out why length is not used
-- local INTERIOR_LENGTHS = {
--     26,
-- }

local INTERIOR_TEXTURE_DIMENSIONS = {
	512,
	-- 1024,
}

local function Initialize()
    for _, dimension in pairs(INTERIOR_TEXTURE_DIMENSIONS) do
        for _, num in pairs(INTERIOR_WIDTHS) do
            local scale = UnitToPixel(num) / dimension
            EnvelopeManager:AddVector2Envelope(
                "interiorfloor"..num,
                {
                    { 0,    { scale, scale } },
                    { 1,    { scale, scale } },
                }
            )
        end
        
        for _, num in pairs(INTERIOR_HEIGHTS) do
            local scale = UnitToPixel(num) / dimension
            EnvelopeManager:AddVector2Envelope(
                "interiorwall"..num,
                {
                    { 0,    { -scale, -scale * 1.064 } }, --(H): why do I seemingly randomly increase height scale by this arbitrary amount? Well, Hamlet's walls are slightly taller too for some reason, I do not know the exact value
                    { 1,    { -scale, -scale * 1.064 } },
                }
            )
            EnvelopeManager:AddVector2Envelope(
                "interiorwallflipped"..num,
                {
                    { 0,    { scale, -scale * 1.064 } },
                    { 1,    { scale, -scale * 1.064 } },
                }
            )
        end
    end
    
    
    -- for _, dimension in pairs(INTERIOR_TEXTURE_DIMENSIONS) do
    --     for _, w in pairs(INTERIOR_WIDTHS) do
    --         for _, l in pairs(INTERIOR_LENGTHS) do
    --             local scale = UnitToPixel(w) / dimension
    --             EnvelopeManager:AddVector2Envelope(
    --                 "interior_floor:" .. w .. "X" .. l .. "(" ..  dimension .. ")",
    --                 {
    --                     { 0,    { scale, scale } },
    --                     { 1,    { scale, scale } },
    --                 }
    --             )
    --         end
    --     end
        
    --     for _, h in pairs(INTERIOR_HEIGHTS) do
    --         local scale = UnitToPixel(num) / dimension
    --         EnvelopeManager:AddVector2Envelope(
    --             "interior_back_wall:" .. w .. "X" .. l .. "(" ..  dimension .. ")",
    --             {
    --                 { 0,    { -scale, -scale * 1.064 } }, --(H): why do I seemingly randomly increase height scale by this arbitrary amount? Well, Hamlet's walls are slightly taller too for some reason, I do not know the exact value
    --                 { 1,    { -scale, -scale * 1.064 } },
    --             }
    --         )
    --         EnvelopeManager:AddVector2Envelope(
    --             "interior_left_wall:" .. w .. "X" .. l .. "(" ..  dimension .. ")",
    --             {
    --                 { 0,    { scale, -scale * 1.064 } },
    --                 { 1,    { scale, -scale * 1.064 } },
    --             }
    --         )
    --         EnvelopeManager:AddVector2Envelope(
    --             "interior_right_wall:" .. w .. "X" .. l .. "(" ..  dimension .. ")",
    --             {
    --                 { 0,    { scale, -scale * 1.064 } },
    --                 { 1,    { scale, -scale * 1.064 } },
    --             }
    --         )
    --     end
    -- end
end

return {
    --Internal use
    Initialize = Initialize,

    --Public use
    INTERIOR_WIDTHS = INTERIOR_WIDTHS,
    INTERIOR_HEIGHTS = INTERIOR_HEIGHTS,
    INTERIOR_TEXTURE_DIMENSIONS = INTERIOR_TEXTURE_DIMENSIONS,
}