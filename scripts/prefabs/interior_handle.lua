local INTERIOR_SHADER = "shaders/interior.ksh"

local EMITTERS = {
    FLOOR = 0,
    LEFT_WALL = 1,
    RIGHT_WALL = 2,
    BACK_WALL = 3,
}

local NUM_EMITTERS = GetTableSize(EMITTERS)

-- Note: Only for initilization -Half
local DIMENSIONS = {"length", "height", "width"} -- length, height, width
local TEXTURES = {"wall", "floor", "minimap"}
local DATA = {"id", "tile", "reverb", "colourcube", "group", "group_x", "group_y", "camera_offset", "camera_zoom"}

local assets =
{
    Asset("SHADER", INTERIOR_SHADER),
}

local function OnSetTexture(inst, name, value)
    inst.textures[name]:set(value)
end

local function OnSetDimension(inst, name, value)
    inst.dimensions[name]:set(value)
end

local function OnSetData(inst, name, value)
    inst.data[name]:set(value)
end

---------------------------------------------------- VFXEffect ------------------------------------------------------

-- Ngl I dont understand the whole "negative" particle lifetime via the SPAWN_LIFETIME_FAST_FORWARD, thats just weird
-- But at least the RANGE makes sense (the lifetime is periodicially reduced so it never ends) - Half
local MAX_LIFETIME = 10000
local SPAWN_LIFETIME_FAST_FORWARD = -10000+10
local SIM_TICK_TIME = TheSim:GetTickTime()
-- DST's lifetime system ensures that if the time alive of a particle stays between -10000 and -9900 it will evaluate correctly, stay between -9990 and -9910 just to be safe
local LIFETIME_RANGE = math.ceil(80/SIM_TICK_TIME)*SIM_TICK_TIME

local MAX_PARTICLES1 = 1
local MAX_PARTICLES2 = 2

 -- TODO: This hornet code is sus, verify it -Half
local MAGIC_NUMBER1 = 0.948
local MAGIC_NUMBER2 = math.tan(math.rad(64.65)) -- (H) this should be 60 degrees(or pi/3) but its... just not I guess? this looks more accurate in-game so um, yea, let's go with it

local function AddParticle(inst, emitter, x, y, z)
	inst.VFXEffect:AddParticle(
        emitter,
        MAX_LIFETIME,   -- lifetime
        x, y, z,        -- position
        0, 0, 0			-- velocity
    )
    inst.VFXEffect:FastForward(0, SPAWN_LIFETIME_FAST_FORWARD)
end

local function Emit(inst)
    -- (H) i am really shit with math, as you can tell.
    -- TODO: This hornetware sucks, make it suck less -Half
    local heightscale = UnitToPixel(inst.dimensions.height:value()) / 512
    -- local scale = UnitToPixel(inst.dimensions.width:value()) / 512
    local height = (PixelToUnit(512 * heightscale)/2) * MAGIC_NUMBER1
    local halflength = inst.dimensions.length:value()/2
    local halfwidth = inst.dimensions.width:value()/2
    local extrawidth = height/MAGIC_NUMBER2

    AddParticle(inst, EMITTERS.FLOOR, 0, 0, 0)
    AddParticle(inst, EMITTERS.LEFT_WALL, -extrawidth, height, -halflength)
    AddParticle(inst, EMITTERS.RIGHT_WALL, -extrawidth, height, halflength)
    AddParticle(inst, EMITTERS.BACK_WALL, -halfwidth-extrawidth, height, 0)
end

local function SetEmitterDimension(inst, emitter, envelope, s1, s2)
    inst.VFXEffect:SetScaleEnvelope(emitter, envelope .. s1)
    inst.VFXEffect:SetUVFrameSize(emitter, -s2/s1, 1)
end

local function SetEmitterTexture(inst, emitter, tex)
    inst.VFXEffect:SetRenderResources(emitter, resolvefilepath(tex), resolvefilepath(INTERIOR_SHADER))
end

local function SetupEmitter(inst, emitter, max_particles, layer, spawn_vectors)
    inst.VFXEffect:SetMaxNumParticles(emitter, max_particles)
    inst.VFXEffect:SetMaxLifetime(emitter, MAX_LIFETIME)
    inst.VFXEffect:SetLayer(emitter, layer)
    inst.VFXEffect:SetSpawnVectors(emitter, unpack(spawn_vectors))
    inst.VFXEffect:SetKillOnEntityDeath(emitter, true)
end

local function VFXEffect_OnDimensionDirty(inst)
    if TheNet:IsDedicated() then return end
    SetEmitterDimension(inst, EMITTERS.FLOOR, "interiorfloor", inst.dimensions.width:value(), inst.dimensions.length:value())
    SetEmitterDimension(inst, EMITTERS.LEFT_WALL, "interiorwallflipped", inst.dimensions.height:value(), inst.dimensions.width:value())
    SetEmitterDimension(inst, EMITTERS.RIGHT_WALL, "interiorwall", inst.dimensions.height:value(), inst.dimensions.width:value())
    SetEmitterDimension(inst, EMITTERS.BACK_WALL, "interiorwall", inst.dimensions.height:value(), inst.dimensions.length:value())
    if inst.ResetParticles then
        inst:ResetParticles(inst)
    end
end

local function VFXEffect_OnTextureDirty(inst)
    if TheNet:IsDedicated() then return end
    SetEmitterTexture(inst, EMITTERS.FLOOR, inst.textures.floor:value())
    SetEmitterTexture(inst, EMITTERS.LEFT_WALL, inst.textures.wall:value())
    SetEmitterTexture(inst, EMITTERS.RIGHT_WALL, inst.textures.wall:value())
    SetEmitterTexture(inst, EMITTERS.BACK_WALL, inst.textures.wall:value())
    -- VFXEffect_OnDimensionDirty(inst) -- TODO: Change envelope based on dimensions? -Half
end

local function VFXEffect_Add(inst)
    if TheNet:IsDedicated() then return end
    inst.entity:AddVFXEffect()
    --

    inst.VFXEffect:InitEmitters(NUM_EMITTERS)
    SetupEmitter(inst,
        EMITTERS.FLOOR, 
        MAX_PARTICLES1, 
        LAYER_GROUND,
        {
            0, 0, -1,
            1, 0, 0
        }
    )
    SetupEmitter(inst,
        EMITTERS.LEFT_WALL, 
        MAX_PARTICLES2, 
        LAYER_WORLD_BACKGROUND,
        {
            1, 0, 0,
            -.5, 1, 0
        }
    )
    SetupEmitter(inst,
        EMITTERS.RIGHT_WALL, 
        MAX_PARTICLES2, 
        LAYER_WORLD_BACKGROUND,
        {
            1, 0, 0,
            -.5, 1, 0
        }
    )
    SetupEmitter(inst,
        EMITTERS.BACK_WALL, 
        MAX_PARTICLES1, 
        LAYER_WORLD_BACKGROUND,
        {
            0, 0, 1,
            -.5, 1, 0
        }
    )
end

local function VFXEffect_Init(inst)
    InteriorManager:AddInteriorHandle(inst)
    if TheNet:IsDedicated() then return end
    Emit(inst)

    local ticktime = 0

    function inst:UpdateParticles(dt)
        -- Make sure our lifetime never ends
        ticktime = ticktime + dt
        if ticktime >= LIFETIME_RANGE then
            for emitter = 0, NUM_EMITTERS, 1 do
                inst.VFXEffect:FastForward(emitter, -LIFETIME_RANGE)
            end
            ticktime = ticktime - LIFETIME_RANGE
        end
    end

    function inst:ResetParticles()
        ticktime = 0
        for emitter = 0, NUM_EMITTERS, 1 do
            inst.VFXEffect:ClearAllParticles(emitter)
        end
        Emit(inst)
    end
end

local function VFXEffect_Terminate(inst)
    InteriorManager:RemoveInteriorHandle(inst)
    if TheNet:IsDedicated() then return end
    inst.UpdateParticles = nil
    inst.ResetParticles = nil
end

----------------------------------------------------- Physics -------------------------------------------------------

local function Physics_OnDimensionDirty(inst)
    inst.Physics:SetRectangle(inst.dimensions.length:value(), inst.dimensions.height:value(), inst.dimensions.width:value())
end

local function Physics_Add(inst)
    inst.entity:AddPhysics()
    inst.Physics:SetMass(0)
    inst.Physics:SetFriction(0)
    inst.Physics:SetDamping(5)
    inst.Physics:SetCollisionGroup(COLLISION.GROUND)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.ITEMS)
    inst.Physics:CollidesWith(COLLISION.FLYERS)
    inst.Physics:CollidesWith(COLLISION.SANITY)
    inst.Physics:SetDontRemoveOnSleep(true)
    inst.Physics:SetActive(false)
end

local function Physics_Init(inst)
    inst.Physics:SetActive(true)
end

local function Physics_Terminate(inst)
    inst.Physics:SetActive(false)
end

---------------------------------------------------- Pathfinding ----------------------------------------------------

local function CreateBarriers(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local _world = TheWorld
 
    for r = -inst.dimensions.length:value()/2, inst.dimensions.length:value()/2 do
        table.insert(inst._pathfinding_barriers, Vector3(x+(inst.dimensions.width:value()/2)+0.5, y, z+r))
        table.insert(inst._pathfinding_barriers, Vector3(x-(inst.dimensions.width:value()/2)-0.5, y, z+r))
    end
    for r = -inst.dimensions.width:value()/2, inst.dimensions.width:value()/2 do
        table.insert(inst._pathfinding_barriers, Vector3(x+r,y,z-(inst.dimensions.length:value() / 2)-0.5))
        table.insert(inst._pathfinding_barriers, Vector3(x+r,y,z+(inst.dimensions.length:value() / 2)+0.5))
    end

    for i,pt in pairs(inst._pathfinding_barriers) do
        _world.Pathfinder:AddWall(pt.x, pt.y, pt.z)
    end
end

local function ClearBarriers(inst)
    local _world = TheWorld
    for i,pt in pairs(inst._pathfinding_barriers) do
        _world.Pathfinder:RemoveWall(pt.x, pt.y, pt.z)
    end
    inst._pathfinding_barriers = {}
end

local function Pathfinding_OnDimensionDirty(inst)
    ClearBarriers(inst)
    CreateBarriers(inst)
end

local function Pathfinding_Add(inst)
    inst._pathfinding_barriers = {}
end

local function Pathfinding_Init(inst)
    CreateBarriers(inst)
end

local function Pathfinding_Terminate(inst)
    ClearBarriers(inst)
end

---------------------------------------------------------------------------------------------------------------------

local function OnDimensionDirty(inst)
    VFXEffect_OnDimensionDirty(inst)
    Physics_OnDimensionDirty(inst)
    Pathfinding_OnDimensionDirty(inst)
end

local function OnTextureDirty(inst)
    VFXEffect_OnTextureDirty(inst)
    -- TODO: Update minimap texture
end

local function OnInitilization(inst)
    OnTextureDirty(inst)
    OnDimensionDirty(inst)

    VFXEffect_Init(inst)
    Physics_Init(inst)
    Pathfinding_Init(inst)

    inst:ListenForEvent("dimensiondirty", OnDimensionDirty)
    inst:ListenForEvent("texturedirty", OnTextureDirty)

    -- tmp for now. interior_handle netvar is nil when unloaded so this just forces a second update when it is loaded.
    if ThePlayer ~= nil then
        ThePlayer:PushEvent("interiorhandledirty")
    end
end

local function OnTermination(inst)
    inst:RemoveEventCallback("dimensiondirty", OnDimensionDirty)
    inst:RemoveEventCallback("texturedirty", OnTextureDirty)

    VFXEffect_Terminate(inst)
    Physics_Terminate(inst)
    Pathfinding_Terminate(inst)
end

-- Allow mods to easliy modify these
local function _init(inst) inst:Init() end
local function _term(inst) inst:Term() end

local function fn()
    local inst = CreateEntity()

    inst:AddTag("NOCLICK")
    inst:AddTag("FX")
    inst:AddTag("interior_handle")

    inst.entity:AddTransform()
	inst.entity:AddNetworkProxy()

    inst.persists = false
	
    inst.dimensions = {}
    for i,v in pairs(DIMENSIONS) do
        inst.dimensions[v] = net_smallbyte(inst.GUID, "interior.dimensions." .. v, "dimensiondirty")
    end

    inst.textures = {}
    for i,v in pairs(TEXTURES) do
        inst.textures[v] = net_string(inst.GUID, "interior.textures." .. v, "texturedirty")
    end

    inst.data = {} -- TODO: Use integers when possible for efficency -Half
    for i,v in pairs(DATA) do
        inst.data[v] = net_string(inst.GUID, "interior.data." .. v)
        inst.data[v]:set_local("")
    end
	--

    VFXEffect_Add(inst)
    Physics_Add(inst)
    Pathfinding_Add(inst)

    inst.Init = OnInitilization
    inst.Term = OnTermination

    inst:DoStaticTaskInTime(0, _init)
    inst.OnRemoveEntity = _term
	
	inst.entity:SetPristine()

    -----------------------------------------------------
    if not TheWorld.ismastersim then
        return inst
    end

    inst.SetTexture = OnSetTexture
    inst.SetDimension = OnSetDimension
    inst.SetData = OnSetData

    return inst
end

return Prefab("interior_handle", fn, assets)