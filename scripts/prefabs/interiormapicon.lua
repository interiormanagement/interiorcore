local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddNetworkProxy()
    inst:AddTag("CLASSIFIED")
    inst:AddTag("interior_minimapentity_classified")

    inst.icon = net_string(inst.GUID, "interiormapicon.icon")
    inst.fog_immune = net_bool(inst.GUID, "interiormapicon.fog_immune")
    inst.priority = net_shortint(inst.GUID, "interiormapicon.priority")

    inst.icon:set("")
    inst.fog_immune:set(false)
    inst.priority:set(0)

    inst.persists = false

    inst.entity:SetPristine()

    return inst
end

return Prefab("interior_minimapentity_classified", fn)
