require("camerashake")
local FollowCamera = require("cameras/followcamera")

local function dummyfn()
end

InteriorCamera = Class(FollowCamera, function(self, inst)
    --Init Interior Variables
	self.interior_pitch = 35 -- 40
	self.interior_heading = 0
	self.interior_distance = 25
	self.interior_currentpos = Vector3(0,0,0)
	self.interior_fov = 35

    self.interior = true

    self.followPlayer = true

    self.time_since_zoom = nil

    FollowCamera._ctor(self, inst, "InteriorCamera")
    
    self.inst = inst
    self.target = nil
    self.currentpos = Vector3(0,0,0)
	self.distance = 30
    self:SetDefault()
end)

-- function InteriorCamera:SetDefaultOffset()
--     self.targetoffset = Vector3(0, 1.5, 0)
-- end

-- function InteriorCamera:SetDefault()
--     self.targetpos = Vector3(0, 0, 0)
--     --self.currentpos = Vector3(0, 0, 0)
--     self:SetDefaultOffset()

--     if self.headingtarget == nil then
--         self.headingtarget = 45
--     end

--     self.fov = 35
--     self.pangain = 4
--     self.headinggain = 20
--     self.distancegain = 1

--     self.zoomstep = 4
--     self.distancetarget = 30
--     --self.lockdistance = nil

--     self.mindist = 15
--     self.maxdist = 50 --40

--     self.mindistpitch = 30
--     self.maxdistpitch = 60--60
--     self.paused = false
--     self.shake = nil
--     self.controllable = true
--     self.cutscene = false

--     if TheWorld and TheWorld:HasTag("cave") then
--         self.mindist = 15
--         self.maxdist = 35
--         self.mindistpitch = 25
--         self.maxdistpitch = 40
--         self.distancetarget = 25
--     end

-- 	if self.gamemode_defaultfn then
-- 		self.gamemode_defaultfn(self)
-- 	end

--     if self.target then
--         self:SetTarget(self.target)
--     end
-- end

function InteriorCamera:GetRightVec()
    local right = (self.interior_heading + 90) * DEGREES
    return Vector3(math.cos(right), 0, math.sin(right))
end

function InteriorCamera:GetDownVec()
    local heading = self.interior_heading * DEGREES
    return Vector3(math.cos(heading), 0, math.sin(heading))
end

function InteriorCamera:GetPitchDownVec()
    local pitch = self.interior_pitch * DEGREES
    local heading = self.interior_heading * DEGREES
    local cos_pitch = -math.cos(pitch)
    local cos_heading = math.cos(heading)
    local sin_heading = math.sin(heading)
    return Vector3(cos_pitch * cos_heading, -math.sin(pitch), cos_pitch * sin_heading)
end

-- function InteriorCamera:SetPaused(val)
-- 	self.paused = val
-- end

-- function InteriorCamera:SetMinDistance(distance)
--     self.mindist = distance
-- end


-- function InteriorCamera:SetGains(pan, heading, distance)
--     self.pangain = pan
--     self.headinggain = heading
--     self.distancegain = distance

-- end

-- function InteriorCamera:GetGains(pan, heading, distance)
--     return self.pangain, self.headinggain, self.distancegain
-- end

-- function InteriorCamera:IsControllable()
--     return self.controllable
-- end

-- function InteriorCamera:SetControllable(val)
--     self.controllable = val
-- end

-- function InteriorCamera:CanControl()
--     return self.controllable
-- end

-- function InteriorCamera:SetOffset(offset)
--     self.targetoffset.x, self.targetoffset.y, self.targetoffset.z = offset.x, offset.y, offset.z
-- end

-- function InteriorCamera:PushScreenHOffset(ref, xoffset)
--     self:PopScreenHOffset(ref)
--     table.insert(self.screenoffsetstack, 1, { ref = ref, xoffset = xoffset })
-- end

-- function InteriorCamera:PopScreenHOffset(ref)
--     for i, v in ipairs(self.screenoffsetstack) do
--         if v.ref == ref then
--             table.remove(self.screenoffsetstack, i)
--             return
--         end
--     end
-- end

-- function InteriorCamera:LockDistance(lock)
--     self.lockdistance = lock or nil
-- end

-- function InteriorCamera:GetDistance()
--     return self.distancetarget
-- end

-- function InteriorCamera:SetDistance(dist)
--     self.distancetarget = dist
-- end

function InteriorCamera:Shake(type, duration, speed, scale)
    return FollowCamera.Shake(self, type or ShakeType.FULL, duration or 1, speed or 0.05, scale or 1)
end

-- function InteriorCamera:SetTarget(inst)
--     self.target = inst
--     self.targetpos.x, self.targetpos.y, self.targetpos.z = self.target.Transform:GetWorldPosition()
--     --self.currentpos.x, self.currentpos.y, self.currentpos.z = self.target.Transform:GetWorldPosition()
-- end

function InteriorCamera:Apply()
    --dir
    local pitch = self.interior_pitch * DEGREES
    local heading = self.interior_heading * DEGREES
    local cos_pitch = math.cos(pitch)
    local cos_heading = math.cos(heading)
    local sin_heading = math.sin(heading)
    local dx = -cos_pitch * cos_heading
    local dy = -math.sin(pitch)
    local dz = -cos_pitch * sin_heading

    --screen horizontal offset
    -- local xoffs, zoffs = 0, 0
    -- if self.currentscreenxoffset ~= 0 then
    --     --FOV is relative to screen height
    --     --hoffs is in units of screen heights
    --     --convert hoffs to xoffs and zoffs in world space
    --     local hoffs = 2 * self.currentscreenxoffset / RESOLUTION_Y
    --     local magic_number = 1.03 -- plz... halp.. if u can figure out what this rly should be
    --     local screen_heights = math.tan(self.fov * .5 * DEGREES) * self.distance * magic_number
    --     xoffs = -hoffs * sin_heading * screen_heights
    --     zoffs = hoffs * cos_heading * screen_heights
    -- end

    --pos
    local px = self.interior_currentpos.x - dx * self.interior_distance
    local py = self.interior_currentpos.y - dy * self.interior_distance
    local pz = self.interior_currentpos.z - dz * self.interior_distance

    TheSim:SetCameraPos(px, py, pz)
    TheSim:SetCameraDir(dx, dy, dz)

    --right
    local right = (self.interior_heading + 90) * DEGREES
    local rx = math.cos(right)
    local ry = 0
    local rz = math.sin(right)

    --up
    local ux = dy * rz - dz * ry
    local uy = dz * rx - dx * rz
    local uz = dx * ry - dy * rx

    TheSim:SetCameraUp(ux, uy, uz)
    TheSim:SetCameraFOV(self.interior_fov)
	
    --listen dist
    local lx, ly, lz
    local listendist = -.1 * self.interior_distance
    if self.followPlayer then
        local target = Vector3(self.interior_currentpos:Get())
	    local source = Vector3(px, py, pz)
	    local dir = target - source	
	    dir:Normalize()
	    local pos = target + dir * listendist
	    lx, ly, lz = pos.x, pos.y, pos.z
    else
        lx =  0.5 * dx * listendist + self.interior_currentpos.x
        ly =  0.5 * dy * listendist + self.interior_currentpos.y
        lz =  0.5 * dz * listendist + self.interior_currentpos.z
    end

    TheSim:SetListener(
        lx, ly, lz, 
        dx, dy, dz, 
        ux, uy, uz
    )
    
end

local lerp = function(lower, upper, t)
   if t > 1 then t = 1 elseif t < 0 then t = 0 end
   return lower*(1-t)+upper*t 
end

local function normalize(angle)
    while angle > 360 do
        angle = angle - 360
    end
    while angle < 0 do
        angle = angle + 360
    end
    return angle
end

-- function InteriorCamera:GetHeading()
--     return self.heading -- Shouldnt this be interior_heading? -Half
-- end

-- function InteriorCamera:GetHeadingTarget()
--     return self.headingtarget
-- end

-- function InteriorCamera:SetHeadingTarget(r)
--     self.headingtarget = r
-- end

function InteriorCamera:ZoomIn(step)
    -- self.distancetarget = math.max(self.mindist, self.distancetarget - (step or self.zoomstep))
    -- self.time_since_zoom = 0
end

function InteriorCamera:ZoomOut(step)
    -- self.distancetarget = math.min(self.maxdist, self.distancetarget + (step or self.zoomstep))
    -- self.time_since_zoom = 0
end

-- function InteriorCamera:Snap()
--     if self.target then
--         local x, y, z = self.target.Transform:GetWorldPosition()
--         self.targetpos.x = x + self.targetoffset.x
--         self.targetpos.y = y + self.targetoffset.y
--         self.targetpos.z = z + self.targetoffset.z
--     else
--         self.targetpos.x, self.targetpos.y, self.targetpos.z = self.targetoffset:Get()
--     end

--     self.currentscreenxoffset = #self.screenoffsetstack > 0 and self.screenoffsetstack[1].xoffset or 0
--     self.currentpos.x, self.currentpos.y, self.currentpos.z = self.targetpos:Get()
--     self.heading = self.headingtarget
--     if not self.lockdistance then
--         self.distance = self.distancetarget
--     end

--     self.pitch = lerp(self.mindistpitch, self.maxdistpitch, (self.distance - self.mindist) / (self.maxdist - self.mindist))

--     self:Apply()
--     self:UpdateListeners(0)
-- end

-- function InteriorCamera:CutsceneMode(b)
--     self.cutscene = b
-- end

-- function InteriorCamera:SetCustomLocation(loc)
--     self.targetpos.x, self.targetpos.y, self.targetpos.z  = loc:Get()
-- end

function InteriorCamera:Update(dt, dontupdatepos)
    if self.paused then
        return
    end

    local pangain = dt * self.pangain

    if not dontupdatepos then
        if self.cutscene then
            self.currentpos.x = lerp(self.currentpos.x, self.targetpos.x + self.targetoffset.x, pangain)
            self.currentpos.y = lerp(self.currentpos.y, self.targetpos.y + self.targetoffset.y, pangain)
            self.currentpos.z = lerp(self.currentpos.z, self.targetpos.z + self.targetoffset.z, pangain)
        else
            -- if self.time_since_zoom and not self.cutscene then
            --     self.time_since_zoom = self.time_since_zoom + dt
            --     if self.should_push_down and self.time_since_zoom > 1.0 then
            --         self.distancetarget = (self.maxdist - self.mindist) * 0.6 + self.mindist
            --     end
            -- end

            if self.target then
                if self.target.components.focalpoint then
                    self.target.components.focalpoint:CameraUpdate(dt)
                end
                local x, y, z = self.target.Transform:GetWorldPosition()
                self.targetpos.x = x + self.targetoffset.x
                self.targetpos.y = y + self.targetoffset.y
                self.targetpos.z = z + self.targetoffset.z
            else
                self.targetpos.x, self.targetpos.y, self.targetpos.z = self.targetoffset:Get()
            end

            self.currentpos.x = lerp(self.currentpos.x, self.targetpos.x, pangain)
            self.currentpos.y = lerp(self.currentpos.y, self.targetpos.y, pangain)
            self.currentpos.z = lerp(self.currentpos.z, self.targetpos.z, pangain)
        end
    end

    -- local screenxoffset = 0
    while #self.screenoffsetstack > 0 do
        -- if self.screenoffsetstack[1].ref.inst:IsValid() then
        --     screenxoffset = self.screenoffsetstack[1].xoffset
        --     break
        -- end
        table.remove(self.screenoffsetstack, 1)
    end
    -- if screenxoffset ~= 0 then
    --     self.currentscreenxoffset = lerp(self.currentscreenxoffset, screenxoffset, pangain)
    -- elseif self.currentscreenxoffset ~= 0 then
    --     self.currentscreenxoffset = lerp(self.currentscreenxoffset, 0, pangain)
    --     if math.abs(self.currentscreenxoffset) < .01 then
    --         self.currentscreenxoffset = 0
    --     end
    -- end

    if not self.interior_currentpos_original then
        self.interior_currentpos_original = {}
        self.interior_currentpos_original.x = self.interior_currentpos.x
        self.interior_currentpos_original.y = self.interior_currentpos.y
        self.interior_currentpos_original.z = self.interior_currentpos.z                
    end     

    self.interior_currentpos.x = lerp(self.interior_currentpos.x, self.interior_currentpos_original.x, pangain)
    self.interior_currentpos.y = lerp(self.interior_currentpos.y, self.interior_currentpos_original.y, pangain)
    self.interior_currentpos.z = lerp(self.interior_currentpos.z, self.interior_currentpos_original.z, pangain)

    if self.shake then
        local shakeOffset = self.shake:Update(dt)
        if shakeOffset then
            local upOffset = Vector3(0, shakeOffset.y, 0)
            local rightOffset = self:GetRightVec() * shakeOffset.x
            self.interior_currentpos.x = self.interior_currentpos.x + upOffset.x + rightOffset.x
            self.interior_currentpos.y = self.interior_currentpos.y + upOffset.y + rightOffset.y
            self.interior_currentpos.z = self.interior_currentpos.z + upOffset.z + rightOffset.z
        else
            self.shake = nil
        end
    end

    self.heading = normalize(self.heading)
    self.headingtarget = normalize(self.headingtarget)

    local diffheading = math.abs(self.heading - self.headingtarget)

    self.heading =
        diffheading <= .01 and
        self.headingtarget or
        lerp(self.heading,
            diffheading <= 180 and
            self.headingtarget or
            self.headingtarget + (self.heading > self.headingtarget and 360 or -360),
            dt * self.headinggain)

    self.distance = math.abs(self.distance - self.distancetarget) > .01 and lerp(self.distance, self.distancetarget, dt * self.distancegain)
					or self.distancetarget

    self.pitch = lerp(self.mindistpitch, self.maxdistpitch, (self.distance - self.mindist) / (self.maxdist - self.mindist))

    self:onupdatefn(dt)
    self:Apply()
    self:UpdateListeners(dt)
end

function InteriorCamera:SetOnUpdateFn(fn)
    self.onupdatefn = fn or function() end
end

return InteriorCamera