local function _OnInteriorChanged(inst)
    print("_OnInteriorChanged", inst, inst.replica.interiorplayer, inst.replica.interiorplayer:GetInteriorHandle())
    inst.replica.interiorplayer:OnInteriorChanged()
end

local InteriorPlayer = Class(function(self, inst)
    self.inst = inst

    self.interior_handle = net_entity(inst.GUID, "interiorplayer._interior_handle", "interiorhandledirty")

    self.inst:DoStaticTaskInTime(0, function()
        self.inst:ListenForEvent("interiorhandledirty", _OnInteriorChanged)
        self:OnInteriorChanged()
    end)
end)

--------------------------------------------------------------------------

function InteriorPlayer:OnInteriorChanged()
    local interior = self.interior_handle:value()
    print("interiorchanged", interior)
    if interior ~= nil then

        if self.inst == ThePlayer then
            SwitchToInteriorCamera(interior)
            SwitchToInteriorEnviroment(interior)
        end

        if not TheWorld.ismastersim then
            self.inst:PushEvent("enterinterior")
        end
    else

        if self.inst == ThePlayer then
            SwitchToOutdoorCamera()
            SwitchToOutdoorEnviroment()
        end

        if not TheWorld.ismastersim then
            self.inst:PushEvent("exitinterior")
        end
    end
end

function InteriorPlayer:InInterior()
    if self.interior_handle then
        return self.interior_handle:value() ~= nil
    end
end

function InteriorPlayer:GetInteriorHandle()
    if self.interior_handle then
        return self.interior_handle:value()
    end
end

return InteriorPlayer
