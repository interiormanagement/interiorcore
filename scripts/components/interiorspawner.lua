--[[
notes:
interiors in hamlet are a size of 15x10 units(usually 1:1.5 ratio, but ROOM_LARGE said fuck you and gave 1:1.444 ratio

ROOM_TINY_WIDTH   = 15,
		ROOM_TINY_DEPTH   = 10,

		ROOM_SMALL_WIDTH  = 18,
		ROOM_SMALL_DEPTH  = 12,

		ROOM_MEDIUM_WIDTH = 24,
		ROOM_MEDIUM_DEPTH = 16,

		ROOM_LARGE_WIDTH  = 26,
		ROOM_LARGE_DEPTH  = 18,

(H): this is a fragile shell of interiors, but it's enough of a layout that you guys are smart enough to complete it(there's probably some things I might still have to do, but otherwise, ik youre clever enough, Half)

TODO:
Minimap Logic
Room Generation + the whole housing renovation stuff
Followers need to teleport
Interior textures only support 512x512 textures(Hamlet will use some of the original tile types like wooden flooring which is 1024x1024)

me(H) things:
some AnimStates will need to be rotated billboards, like in Hamlet. A default shader can probably do this task.
Whatever other shader stuff needs to be done for the interiors
]]

--------------------------------------------------------------------------
--[[ Dependencies ]]
--------------------------------------------------------------------------

local math = math

--------------------------------------------------------------------------
--[[ InteriorSpawner class definition ]]
--------------------------------------------------------------------------
return Class(function(self, inst)
    
    --------------------------------------------------------------------------
    --[[ Public Member Variables ]]
    --------------------------------------------------------------------------
    
    self.inst = inst
    
    --------------------------------------------------------------------------
    --[[ Private Member Variables ]]
    --------------------------------------------------------------------------
    
    local _world = TheWorld
    
    local taken_positions = {} --[pos_index] = true
    
    local interiors = {} --[id] = data
    local active_interiors = {} --[id] = interiors[id]
    local cached_player_interiors = {} --[userid] = interior_id
    local interior_room_positions = {} --[interior_group] = {[x][y] = interior_id}, same interior can be set to multiple positions, def or id?
    local next_interior_ID = 0
    
    --------------------------------------------------------------------------
    --[[ Private Member functions ]]
    --------------------------------------------------------------------------
    
    local function CreateInteriorHandle(active_interior)
        active_interior.handle = InteriorManager:CreateInteriorHandle(active_interior)
    end
    
    local function ClearInteriorHandle(active_interior)
        active_interior.handle:Remove()
        active_interior.handle = nil
    end
    
    local function SpawnProp(prefab, interior)
        local ent = SpawnPrefab(prefab.name)
    
        ent.Transform:SetPosition((interior.world_position + (prefab.pos_offset or Vector3(0,0,0))):Get())
        ent.Transform:SetRotation(prefab.rotation or 0)
        
        for _, tag in pairs(prefab.addtags or {}) do
            ent:AddTag(tag)
        end
        
        return ent
    end
    
    local function SpawnPendingProps(interior)
        --Spawn our pending props once, these will now be in object_list
        print("Spawning our pending props for "..interior.interior_id)
        
        for i, prefab in pairs(interior.pending_props) do
            local prop = SpawnProp(prefab, interior)
            interior.object_list[prop] = true
        end
        
        --Hornet: Done spawning, goodbye props
        interior.pending_props = nil
    end
    
    local function ChuckObjectIntoLimbo(obj, interior)
        if not obj.persists then
            --This object isn't meant to persist anyways, chuck it out
            obj:Remove()
            interior.object_list[obj] = nil
            return
        end
        
        obj:AddTag("INTERIOR_LIMBO")
    
        if obj.SoundEmitter then
            obj.SoundEmitter:OverrideVolumeMultiplier(0)
        end
        
        if obj.Physics and not obj.Physics:IsActive() then
            obj.dissablephysics = true			
        end
        
        if obj.RemovedFromInteriorScene then
            --Hornet: Making sure that we are NOT touching interior data from a prefab
            obj:RemovedFromInteriorScene(deepcopy(interior))
        end
    
        for k, v in pairs(obj.components) do
            if v and type(v) == "table" and v.RemovedFromInteriorScene then
                v:RemovedFromInteriorScene(deepcopy(interior))
            end
        end
        
        local pos_offset = obj:GetPosition()-interior.world_position
        print("Our offset isss ", pos_offset)
        local pos = InteriorManager:GetSpawnPosition() + pos_offset 
        print(pos)
        
        obj:RemoveFromScene()
        obj.Transform:SetPosition(pos:Get())
    end
    
    local function RescueObjectFromLimbo(obj, interior)
        if not obj.persists then
            --This already should've been handled in ChuckObjectIntoLimbo... but just in case?
            obj:Remove()
            interior.object_list[obj] = nil
            return
        end
        
        obj:RemoveTag("INTERIOR_LIMBO")
    
        if obj.SoundEmitter then
            obj.SoundEmitter:OverrideVolumeMultiplier(1)
        end
    
        if obj.dissablephysics then
            obj.dissablephysics = nil
            obj.Physics:SetActive(false)
        end
        
        if obj.ReturnedToInteriorScene then
            obj:ReturnedToInteriorScene(deepcopy(interior))
        end
    
        for k, v in pairs(obj.components) do
            if v and type(v) == "table" and v.ReturnedToInteriorScene then
                v:ReturnedToInteriorScene(deepcopy(interior))
            end
        end
        
        local pos_offset = obj:GetPosition() - InteriorManager:GetSpawnPosition()
        print("Rescuin!", pos_offset)
        local pos = interior.world_position + pos_offset
        print(pos)
    
        obj:ReturnToScene()
        obj.Transform:SetPosition(pos:Get())
    end

    local function GetNextInteriorSpawnPos()
        for i = 0, GetTableSize(taken_positions)+2, 1 do
            if taken_positions[i] == nil then
                local pos = InteriorManager:GetSpawnPosition()
                pos.z = pos.z + i * InteriorManager:GetMaxSize()
                return pos, i
            end
        end
    end
    
    --------------------------------------------------------------------------
    --[[ Public Member functions ]]
    --------------------------------------------------------------------------

    function self:EnterInterior(doer, interior_id, dest, skipunloadattempt)
        local interior = interiors[interior_id]
        
        if not skipunloadattempt then
            local cached_interior = cached_player_interiors[doer.userid]
            
            self.inst:DoTaskInTime(0, function()
                if cached_interior and self:ShouldUnloadInterior(cached_interior) then
                    print("unloading")
                    self:UnloadInterior(cached_interior)
                end
            end)
        end
        
        cached_player_interiors[doer.userid] = interior_id
        
        if self:ShouldLoadInterior(interior_id) then
            self:LoadInterior(interior_id)
        end
        
        doer:SetInteriorID(interior_id)

        if dest ~= doer then
            local pt = Vector3(interior.world_position:Get())
            if dest and dest.prefab then
                pt = dest:GetPosition()
            elseif dest then
                pt = dest
            end
            if doer.Physics ~= nil then
                doer.Physics:Teleport(pt:Get())
            elseif doer.Transform ~= nil then
                doer.Transform:SetPosition(pt:Get())
            end
            if _world.components.walkableplatformmanager then -- NOTES(JBK): Workaround for teleporting too far causing the client to lose sync.
                _world.components.walkableplatformmanager:PostUpdate(0)
            end
        end

        if doer.components.interiorplayer then
            doer.components.interiorplayer:EnterInterior(interior.handle)
        end
        -- TODO: Support for non players
    end

    function self:ExitInterior(doer, interior_id, dest)
        local interior = interiors[interior_id]
        
        cached_player_interiors[doer.userid] = nil
        
        doer:SetInteriorID(nil)

        if dest ~= doer then
            local pt = Vector3(TheWorld.components.playerspawner:GetAnySpawnPoint()) --florid postern if no location found...
            if dest and dest.prefab then
                pt = dest:GetPosition()
            elseif dest then
                pt = dest
            end
            if doer.Physics ~= nil then
                doer.Physics:Teleport(pt:Get())
            elseif doer.Transform ~= nil then
                doer.Transform:SetPosition(pt:Get())
            end
            if _world.components.walkableplatformmanager then -- NOTES(JBK): Workaround for teleporting too far causing the client to lose sync.
                _world.components.walkableplatformmanager:PostUpdate(0)
            end
        end

        if self:ShouldUnloadInterior(interior_id) then
            self:UnloadInterior(interior_id)
        end

        print("exit interior")
        if doer.components.interiorplayer then
            print("exit interior player")
            doer.components.interiorplayer:ExitInterior()
        end
    end

    function self:LoadInterior(interior_id)
        print("LoadInterior", GetTime())
        if active_interiors[interior_id] then print("[Interior_Manager] Tried running Load Interior, "..interior_id.." is already loaded!") return end
        --
        local interior = interiors[interior_id]
        interior.world_position, interior.position_index = GetNextInteriorSpawnPos()
        taken_positions[interior.position_index] = interior
        print("Loading interior "..interior.interior_id..". Part of group: ("..(interior.interior_group or "UNKNOWN GROUP")..")")

        active_interiors[interior_id] = interior
        
        if interior.pending_props then
            SpawnPendingProps(interior)
        else
            for obj, _ in pairs(interior.object_list) do
                RescueObjectFromLimbo(obj, interior)
            end
        end

        CreateInteriorHandle(active_interiors[interior_id])
        self:GetInteriorEntities(interior_id)
    end

    function self:UnloadInterior(interior_id)
        if active_interiors[interior_id] == nil then print("[Interior_Manager] Tried running Unload Interior, "..interior_id.." is already NOT loaded!") return end
        --
        local interior = interiors[interior_id]
        print("Unload interior "..interior.interior_id..". Part of group: ("..(interior.interior_group or "UNKNOWN GROUP")..")")

        self:GetInteriorEntities(interior_id)
        
        for obj, _ in pairs(interior.object_list) do
            ChuckObjectIntoLimbo(obj, interior)
        end
        
        ClearInteriorHandle(active_interiors[interior_id])
        
        taken_positions[interior.position_index] = nil
        interior.world_position = nil

        active_interiors[interior_id] = nil
    end

    --[[
    --Hornet: Named parameters instead of positional parameters because holy moly there is too many parameters for me to want to remember each position
    interior_data = {
        length = 15, the length
        width = 10, the width
        interior_group = group_id, the group this interior belongs to, for minimapping logic(and etc)
        interior_id = id, the unique id that this specific interior holds, and only this interior
        pending_props = { { name = prefabname, pos_offset = Vector3(0,0,0), rotation = 0, addtags = {"sus"}} }, a table enlisting the prefabs that need to spawn on first load of the interior
        
        floortexture = "levels/textures/noise_woodfloor.tex",
        walltexture = "levels/textures/interiors/shop_wall_woodwall.tex",
        minimaptexture = "levels/textures/map_interior/mini_ruins_slab.tex",
        
        cc = "images/colour_cubes/pigshop_interior_cc.tex",
        reverb = "inside",
    }
    local id = TheWorld.components.interiorspawner:GetNewID() 
    TheWorld.components.interiorspawner:CreateRoom({interior_id = id, floortexture = INTERIOR_FLOOR, walltexture = INTERIOR_WALL}) print(id) 
    TheWorld.components.interiorspawner:EnterInterior(ThePlayer, id)

    object_list = {
        [ent] = true,
    }
    ]]

    -- Cringe, maybe just use a single default
    local function GetDefaultCameraSettings(width)
        if width == 12 then return -2, 25 end   --12x18
        if width == 16 then return -1.5, 30 end --16x24
        if width == 18 then return 2, 35 end    --18x26
        return -2.5, 23                         --10x15
    end

    local function SetDefault(tbl, k, v)
        if tbl[k] == nil then tbl[k] = v end
    end

    function self:RetrofitInterior(interior_def)
        SetDefault(interior_def, "dimensions", {})
        SetDefault(interior_def.dimensions, "length", 15)
        SetDefault(interior_def.dimensions, "width", 10)
        SetDefault(interior_def.dimensions, "height", 5)

        SetDefault(interior_def, "textures", {})
        SetDefault(interior_def.textures, "floor", "levels/textures/interiors/sourceerror.tex")
        SetDefault(interior_def.textures, "wall", "levels/textures/interiors/sourceerror.tex")
        SetDefault(interior_def.textures, "minimap", "mini_sourceerror.tex")

        SetDefault(interior_def, "pending_props", {})

        SetDefault(interior_def, "cc", "images/colour_cubes/pigshop_interior_cc.tex")
        SetDefault(interior_def, "reverb", "inside")
        SetDefault(interior_def, "pos", {x = 0, y = 0})
        SetDefault(interior_def, "tile", "INTERIOR")

        local default_offset, default_zoom = GetDefaultCameraSettings(interior_def.dimensions.width)
        SetDefault(interior_def, "camera_offset", default_offset)
        SetDefault(interior_def, "camera_zoom", default_zoom)

        SetDefault(interior_room_positions, interior_def.interior_group, {})
        SetDefault(interior_room_positions[interior_def.interior_group], interior_def.pos.x, {})
        SetDefault(interior_room_positions[interior_def.interior_group][interior_def.pos.x], interior_def.pos.y, {})
    end

    function self:CreateRoom(interior_data)
        local interior_def = {
            dimensions = {
                length = interior_data.length,
                width = interior_data.width,
                height = interior_data.height,
            },
            textures = {
                floor = interior_data.floortexture,
                wall = interior_data.walltexture,
                minimap = interior_data.minimaptexture,
            },
            interior_group = interior_data.interior_group,
            interior_id = interior_data.interior_id,
            pending_props = interior_data.pending_props,
            object_list = {},

            camera_offset = interior_data.cameraoffset,
            camera_zoom = interior_data.zoom,
            
            cc = interior_data.cc,
            reverb = interior_data.reverb,
            pos = interior_data.pos,
            tile = interior_data.tile,
        }

        self:RetrofitInterior(interior_def)
        
        interiors[interior_def.interior_id] = interior_def
        print(interior_def.interior_group, interior_data.pos.x, interior_data.pos.y)
        interior_room_positions[interior_def.interior_group][interior_data.pos.x][interior_data.pos.y] = interior_def.interior_id -- def or id?
    end
    --TODO(H): unfinished function, needs to be lot more clean up here
    function self:RemoveRoom(interior_id)
        interiors[interior_id] = nil
    end

    function self:ShouldLoadInterior(interior_id)
        return active_interiors[interior_id] == nil
    end

    function self:ShouldUnloadInterior(interior_id)
        return active_interiors[interior_id] and
            (#self:GetPlayersInInterior(interior_id) <= 0)
    end

    function self:GetNewID()
        next_interior_ID = next_interior_ID + 1
        return next_interior_ID
    end

    function self:GetInteriorAtPoint(x, y, z)
        local handle = InteriorManager:GetInteriorHandleAtPoint(x, z)
        if handle then
            return true, handle.data.id:value()
        end
        return false
    end
    
    function self:GetPlayersInInterior(interior_id)
        local active_interior = active_interiors[interior_id]
        --unloaded, return an empty list
        if active_interior == nil then return {} end
        return InteriorManager:FindPlayersInInterior(active_interior.handle)
    end
    
    local INTERIOR_IMMUNE_TAGS = {"player", "INTERIOR_LIMBO_IMMUNE", "INLIMBO"}

    function self:GetInteriorEntities(interior_id)
        local active_interior = active_interiors[interior_id]
        --unloaded, return object_list
        if active_interior == nil then return interiors[interior_id].object_list end
        --todo(h): there needs to be clean up for old object list

        local ents = InteriorManager:FindEntitiesInInterior(active_interior.handle)
    
        for i = #ents, 1, -1 do
            local ent = ents[i]
            ent:SetInteriorID(interior_id)
    
            -- TODO: Why not add these to the INTERIOR_CANT_TAGS? - Half
            -- Do we need to set the id for them?
            if ent:HasOneOfTags(INTERIOR_IMMUNE_TAGS) or ent.entity:GetParent() then
                table.remove(ents, i)
            end
        end
    
        for k, ent in pairs(ents) do
            active_interior.object_list[ent] = true
        end
    
        return active_interior.object_list
    end
    
    function self:GetDebugString()
        --[[
        TODO(H): include debug names for interiors?
        ]]
        local str = "\n[[\n\n"
    
        for id, data in pairs(active_interiors) do
            str = str.."\tInterior ID    : "..id.."\n"
            str = str.."\tInterior Group : "..data.interior_group.."\n"
    
            str = str.."\tRoom Length    : "..data.length.."\n"
            str = str.."\tRoom Width     : "..data.width.."\n"
            str = str.."\tRoom Height    : "..data.height.."\n"
    
            str = str.."\tFlooring       : "..data.floortexture.."\n"
            str = str.."\tWalling        : "..data.walltexture.."\n"
            str = str.."\tMiniMapping    : "..data.minimaptexture.."\n"
    
            str = str.."\tReverb         : "..data.reverb.."\n"
            str = str.."\tCC         	 : "..data.cc.."\n"
    
            str = str.."\tPlayers:\n"
    
            for k, v in pairs(AllPlayers) do
                if v:IsValid() and v:GetInteriorID() == id then
                    str = str.."\t\t"..v:GetDisplayName().."\n"
                end
            end
    
            str = str.."-----------------------\n\n"
        end
    
        str = str.."\n]]"
    
        return str
    end
    
    --------------------------------------------------------------------------
    --[[ Private event handlers ]]
    --------------------------------------------------------------------------
    
    local function OnPlayerJoined(src, player)
        --Load into interior, and blahblah such stuff.
        if player == nil or (player.userid and player.userid == "") then
            return
        end
    
        local name = UserToName(player.userid)
        if name then
            print("[Interior_Manager] "..name.. " joined the server. Were they in an interior last time? Let's chuck em in if so!")
            local cached_interior = cached_player_interiors[player.userid]
    
            if cached_interior == nil then return end
            if interiors[cached_interior] == nil then
                --(H): This interior doesn't exist anymore, so for now, we're chucking them back to the Florid Postern.
                print("[Interior_Manager "..name.."'s cached interior didn't exist. We chucked them to the Florid Postern.")
                player.Transform:SetPosition(_world.components.playerspawner:GetAnySpawnPoint())
                return
            end
            
            print("Throwing em into the interior")
            --TODO(H): I think player_classified isn't set in time fast enough
            self:EnterInterior(player, cached_interior, nil, true)
        end
    end
    
    local function OnPlayerLeft(src, player)
        if player == nil or (player.userid and player.userid == "") then
            return
        end
    
        local name = UserToName(player.userid)
        if name then
            local cached_interior = cached_player_interiors[player.userid]
               print("[InteriorSpawner] "..name.. " left the server. Let's unload their interior if need be: ", cached_interior)
    
            if cached_interior == nil then return end
            if self:ShouldUnloadInterior(cached_interior) then
                self:UnloadInterior(cached_interior)
            end
        end
    end
    
    --------------------------------------------------------------------------
    --[[ Save/Load ]]
    --------------------------------------------------------------------------
    
    function self:OnSave()
        local data = {}
        local ents = {}
    
        data.interiors = deepcopy(interiors)
        data.object_list = {} --[id] = { objGUID, objGUID2 }
    
        for id, interior in pairs(data.interiors) do
            data.object_list[id] = {}
    
            for obj, _ in pairs(self:GetInteriorEntities(id)) do
                if obj.persists then
                    print("Saving", obj, "in interior")
                    table.insert(data.object_list[id], obj.GUID)
                    table.insert(ents, obj.GUID)
                end
            end
            
            data.interiors[id].object_list = {}
            data.interiors[id].handle = nil --these don't persist, no need to save
        end
        
        data.next_interior_ID = next_interior_ID
        data.cached_player_interiors = cached_player_interiors
        data.interior_room_positions = interior_room_positions
    
        return ZipAndEncodeSaveData(data), ents --TODO, zip and encode ents?
    end
    
    function self:OnLoad(data)
        data = DecodeAndUnzipSaveData(data)
        if data == nil then
            return
        end
    
        interiors = data.interiors or {}
        
        for id, interior in pairs(interiors) do
            self:RetrofitInterior(interiors[id])
            interiors[id].object_list = {}
        end
    
        next_interior_ID = data.next_interior_ID or 0
        cached_player_interiors = data.cached_player_interiors or {}
        interior_room_positions = data.interior_room_positions or {}
    end
    
    function self:LoadPostPass(ents, data)
        data = DecodeAndUnzipSaveData(data)
        if data == nil then
            return
        end
    
        --Hornet: me praying to the LUA goddess(i need the continue statement)
        print("LoadPostPass", GetTime())
        print(data.object_list)
        for id, obj_list in pairs(data.object_list or {}) do
            print(id, obj_list)
            print(interiors[id])
            if interiors[id] then
                print("Hey there we passed?")
                printwrap("Our object list", obj_list)
                for _, obj_id in pairs(obj_list) do
                    print("object id: ", obj_id)
                    if obj_id and ents[obj_id] then
                        local object = ents[obj_id].entity
                        print(object)
                        interiors[id].object_list[object] = true
                    end
                end
            end
        end
    end
    
    --------------------------------------------------------------------------
    --[[ Initialization ]]
    --------------------------------------------------------------------------
    
    inst:ListenForEvent("ms_playerjoined", OnPlayerJoined)
    inst:ListenForEvent("ms_playerleft", OnPlayerLeft)
    
    end)