local InteriorPlayer = Class(function(self, inst)
    self.inst = inst
end)

-- Save/load currently handled by the InteriorSpawner
-- function InteriorPlayer:OnSave()

-- end

-- function InteriorPlayer:OnLoad(data)

-- end

function InteriorPlayer:EnterInterior(interior_handle)
    print("player entering interior", interior_handle)
	if self.inst.replica.interiorplayer then
		self.inst.replica.interiorplayer.interior_handle:set(interior_handle)
	end

    self.inst:PushEvent("enterinterior")
end

function InteriorPlayer:ExitInterior()
    print("exit interior", self.inst.replica.interiorplayer)
	if self.inst.replica.interiorplayer then
		self.inst.replica.interiorplayer.interior_handle:set(nil)
	end

    self.inst:PushEvent("exitinterior")
end

function InteriorPlayer:InInterior()
    if self.inst.replica.interiorplayer then
        return self.inst.replica.interiorplayer.interior_handle:value() ~= nil
    end
end

function InteriorPlayer:GetInteriorHandle()
    if self.inst.replica.interiorplayer then
        return self.inst.replica.interiorplayer.interior_handle:value()
    end
end

return InteriorPlayer

