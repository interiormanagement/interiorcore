local Widget = require "widgets/widget"
local Image = require "widgets/image"

-- Class that handles an objects minimap icon
local ObjectIcon = Class(Image, function(self, atlas, tex)
    Image._ctor(self, atlas, tex)

    self.fog_immune = false

    self:SetScale(MINIMAP.ICON_SCALE, MINIMAP.ICON_SCALE)
end)

function ObjectIcon:SetRoomPercentPos(room, perc_x, perc_y)
    local int_x, _, int_y = room:GetPositionXYZ()
    local int_pixel_length, int_pixel_width = room.roomicon:GetRoomPixelSize()
    local lengthoffset, widthoffset = -int_pixel_length/2, -int_pixel_width/2

    self:SetPosition(int_x + perc_x * int_pixel_length + lengthoffset, int_y + perc_y * int_pixel_width + widthoffset)
end

function ObjectIcon:SetFogImmune(fog_immune)
    self.fog_immune = fog_immune
end

function ObjectIcon:SetFogOfWar(enable_fog)
    if enable_fog and not self.fog_immune then
        self.bg:SetTint(.4, .4, .4, 0)
    else
        self.bg:SetTint(1, 1, 1, 1)
    end
end

function ObjectIcon:AddDoor(doordirection)
    local int_x, _, int_y = self:GetPositionXYZ()
    local int_pixel_length, int_pixel_width = self.roomicon:GetRoomPixelSize()
    -- Just for testing atm
    if doordirection then
        if doordirection == "west" or doordirection == "east" then
            self.arrow = self:AddChild(Image("levels/textures/map_interior/passage.xml", "passage.tex"))
            self.arrow:SetScale(1.125, 1.1)
            if doordirection == "west" then
                self.arrow:SetPosition(int_x - (int_pixel_length + MINIMAP.ROOM_SPACING)/2, 0)
            else
                self.arrow:SetPosition(int_x + (int_pixel_length + MINIMAP.ROOM_SPACING)/2, 0)
            end
        elseif doordirection == "north" or doordirection == "south" then
            self.arrow = self:AddChild(Image("levels/textures/map_interior/passage.xml", "passage.tex"))
            self.arrow:SetScale(1.1, 1.125)
            self.arrow:SetRotation(90)
            if doordirection == "north" then
                self.arrow:SetPosition(0, int_y + (int_pixel_width + MINIMAP.ROOM_SPACING)/2)
            else
                self.arrow:SetPosition(0, int_x - (int_pixel_width - MINIMAP.ROOM_SPACING)/2)
            end
        end
    end
end

return ObjectIcon