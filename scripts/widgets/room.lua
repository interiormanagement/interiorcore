local Widget = require "widgets/widget"
local Image = require "widgets/image"
local RoomIcon = require "widgets/roomicon"
local ObjectIcon = require "widgets/objecticon"

-- Class that handles a single interior room on the map
local Room = Class(Widget, function(self, name, length, width)
    Widget._ctor(self, "Room")
    self.roomicon = self:AddChild(RoomIcon(GetMiniMapIconAtlas(name), name))
    self.roomicon:SetRoomSize(length, width)

    self.objicons = {}
end)

function Room:AddMiniMapIcon(name, perc_x, perc_y, fog_immune, doordirection)
    local icon = self:AddChild(ObjectIcon(GetMiniMapIconAtlas(name), name))
    icon:SetRoomPercentPos(self, perc_x, perc_y)
    icon:SetFogImmune(fog_immune)
    if doordirection then
        icon:AddDoor(doordirection)
    end
    
    --render is not accurate, I think it should have BLENDMODE.Additive but this causes issues overlaying with the bg additive
    --todo(h): eh this implementation will change
    --todo(h): stuff like locked/unknown passage icons
    
    table.insert(self.objicons, icon)
end

function Room:SetFogOfWar(enable_fog) -- SetOccupied
    self.roomicon:SetFogOfWar(enable_fog)
    for i, obj in pairs(self.objicons) do
        obj:SetFogOfWar(enable_fog)
    end
end

function Room:SetRoomPosition(x, y)
    local int_pixel_length, int_pixel_width = self.roomicon:GetRoomPixelSize()
    self:SetPosition(int_pixel_length * x + MINIMAP.ROOM_SPACING * x, int_pixel_width * y + MINIMAP.ROOM_SPACING * y)
end

return Room