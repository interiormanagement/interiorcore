local Widget = require "widgets/widget"
local Image = require "widgets/image"

--you might think i performed some brilliant mathematics to figure out this number that is totally precise to hamlet
--nah i pulled it out of my ass and said "good enough" teehee -Hornet
local MAP_SCALE = 1.77 / 10 -- magic number 4... -Half

local BG_PIXEL_W, BG_PIXEL_H = 64, 64
local FRAME_PIXEL_W, FRAME_PIXEL_H = 231, 218 -- Dimensions if trimmed to remove whitespace...
local FRAME_BORDER_X1, FRAME_BORDER_X2, FRAME_BORDER_Y1, FRAME_BORDER_Y2 = 29, 226, 39, 214
local TARG_FRAME_PIXEL_W, TARG_FRAME_PIXEL_H = FRAME_BORDER_X2 - FRAME_BORDER_X1, FRAME_BORDER_Y2 - FRAME_BORDER_Y1
local TARG_FRAME_W, TARG_FRAME_H = BG_PIXEL_W + (FRAME_PIXEL_W - TARG_FRAME_PIXEL_W)/2, BG_PIXEL_H + (FRAME_PIXEL_H - TARG_FRAME_PIXEL_H)/2

-- Class that handles an interiors room minimap icon
local RoomIcon = Class(Widget, function(self, atlas, tex)
    Widget._ctor(self, "RoomIcon")

    self.bg = self:AddChild(Image(atlas, tex))
    self.bg:SetUVMode(WRAP_MODE.WRAP)

    self.frame = self:AddChild(Image("levels/textures/map_interior/frame.xml", "frame.tex")) -- TODO: Custom frame?
    self.frame:SetSize(TARG_FRAME_W, TARG_FRAME_H) -- Still incorrect ugh -Half 
end)

function RoomIcon:SetRoomImage(atlas, tex)
    self.bg:SetTexture(atlas, tex)
end

function RoomIcon:SetRoomSize(length, width)
    local test = 1/2 --TODO: Make the UV scale more accurate to DS -Half
    self.bg:SetUVScale(test*(-3 * length / 10), test*(-2 * width / 10)) -- 1, 1 = one repeat, 0, 0 = 2 x 2, -1, -1 = 3 x 3
    self:SetScale(length * MAP_SCALE, width * MAP_SCALE)
end

function RoomIcon:GetRoomPixelSize()
    local w1, h1 = self:GetLooseScale()
    return BG_PIXEL_W*w1, BG_PIXEL_H*h1
end

function RoomIcon:SetFogOfWar(enable_fog)
    if enable_fog then
        self.bg:SetTint(.4, .4, .4, 0)
    else
        self.bg:SetTint(1, 1, 1, 1)
    end
end

return RoomIcon