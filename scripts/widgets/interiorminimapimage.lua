local unpack = unpack
local Widget = require "widgets/widget"

-- Public API to manage the interior minimap handle
-- Similar to how the real minimap uses an image widget to manage the minimap handle
local InteriorMiniMapImage = Class(Widget, function(self)	
    Widget._ctor(self, "InteriorMiniMapImage")
--     [01:32:47]: ImageWidget	SetHAnchor	function: 00000000983A2F00	
--     [01:32:47]: ImageWidget	SetAlphaRange	function: 00000000983A37C0	
--     [01:32:47]: ImageWidget	SetTint	function: 00000000983A2F40	
--     [01:32:47]: ImageWidget	SetBlendMode	function: 00000000983A3B00	
--     [01:32:47]: ImageWidget	SetVAnchor	function: 00000000983A2CC0	
--     [01:32:47]: ImageWidget	SetRadiusForRayTraces	function: 00000000983A3240	

    self._handle = nil
    self._size_x, self._size_y = 0, 0
end)

function InteriorMiniMapImage:SetSize(w, h)
    self._size_x, self._size_y = w, h
    self:SetScissor(-w/2, -h/2, w, h)
end

function InteriorMiniMapImage:GetSize(w, h)
    return self._size_x, self._size_y
end

function InteriorMiniMapImage:SetInteriorHandle(handlefn)
    local minimap_handle = handlefn()
    self._handle = minimap_handle:AttachImage(self)
end

-- Note: Overrides normal scale unlike ImageWidget
function InteriorMiniMapImage:SetUVScale(xScale, yScale)
    self:SetScale(1/xScale, 1/yScale)
end

function InteriorMiniMapImage:SetTint(r, g, b, a)
    -- TODO: Tint all children
    self.tint = {r, g, b, a}
end

return InteriorMiniMapImage