local InteriorManager = Class(function(self, inst)

--------------------------------------------------------------------------
--[[ Dependencies ]]
--------------------------------------------------------------------------

local DIMENSION_DEFS = require("interior_defs/dimension_defs")

local sim = TheSim
local math = math
local FindPlayersInRange = FindPlayersInRange

--------------------------------------------------------------------------
--[[ Private constants ]]
--------------------------------------------------------------------------

--TODO, scale with world size. for now, 2048x2048 world makers will be eating L's
local _interior_spawn_origin = nil
local _interior_storage_origin = nil--welcome to the dumping facility

--At most *possible* max, there can be 64 active interiors at a time as there can be 64 max players in a shard. We are unlikely to push to even a quarter of this limit in normal play however.
local MAX_INTERIOR_SIZE = TUNING.MAX_INTERIOR_SIZE

--------------------------------------------------------------------------
--[[ Public Member Variables ]]
--------------------------------------------------------------------------

self.inst = inst

--------------------------------------------------------------------------
--[[ Private Member Variables ]]
--------------------------------------------------------------------------

local _ismastersim = TheNet:GetIsMasterSimulation()
local _isdedicated = TheNet:IsDedicated()
local _world = TheWorld
local _map = _world.Map

local _handlers = {}

--------------------------------------------------------------------------
--[[ Private Member functions ]]
--------------------------------------------------------------------------

local function PointInsideAABB(box, px, py)
	return px >= box.minX and
		px <= box.maxX and
		py >= box.minY and
		py <= box.maxY
end

--------------------------------------------------------------------------
--[[ Public Member functions ]]
--------------------------------------------------------------------------

if _ismastersim then
    function self:CreateInteriorHandle(active_interior)
        local handle = SpawnPrefab("interior_handle")

        handle.Transform:SetPosition(active_interior.world_position:Get())

        for k,v in pairs(active_interior.textures) do
            handle:SetTexture(k, v)
        end

        for k,v in pairs(active_interior.dimensions) do
            handle:SetDimension(k, v)
        end
        
        handle:SetData("id", active_interior.interior_id)
        handle:SetData("reverb", active_interior.reverb)
        handle:SetData("colourcube", active_interior.cc)
        handle:SetData("group", active_interior.interior_group)
        handle:SetData("group_x", active_interior.pos.x)
        handle:SetData("group_y", active_interior.pos.y)
        handle:SetData("tile", active_interior.tile)
        handle:SetData("camera_offset", active_interior.camera_offset)
        handle:SetData("camera_zoom", active_interior.camera_zoom)

        return handle
    end
end

function self:AddInteriorHandle(handle)
    _handlers[handle] = true
end

function self:RemoveInteriorHandle(handle)
    _handlers[handle] = nil
end

function self:IsPointInInterior(handle, x, z)
    local int_x, int_y, int_z = handle.Transform:GetWorldPosition()
    local halfwidth = handle.dimensions.width:value()/2
    local halflength = handle.dimensions.length:value()/2
    local AABB = {
        minX = int_x - halfwidth,
        maxX = int_x + halfwidth,
        minY = int_z - halflength,
        maxY = int_z + halflength,
    }
    if PointInsideAABB(AABB, x, z) then
        return true
    end
end

if _ismastersim then
    function self:GetInteriorHandleAtPoint(x, z)
        -- Cringe
        -- TODO: Calculate position index to get interior from spawner
        for interior_handle, _ in pairs(_handlers) do
            if self:IsPointInInterior(interior_handle, x, z) then
                return interior_handle, interior_handle.data.id:value()
            end
        end
    end
else
    function self:GetInteriorHandleAtPoint(x, z)
        local _player = ThePlayer
        local interior_handle = _player and _player.replica.interiorplayer:GetInteriorHandle()
        if interior_handle and self:IsPointInInterior(interior_handle, x, z) then
            return interior_handle, interior_handle.data.id:value()
        end
    end
end

function self:IsPointInInteriorSpawn(x, z)
    return x + MAX_INTERIOR_SIZE >= _interior_spawn_origin.x
end

function self:IsPointInInteriorStorage(x, z)
    return z + MAX_INTERIOR_SIZE >= _interior_storage_origin.z
end

function self:FindPlayersInInterior(handle)
    local x, y, z = handle.Transform:GetWorldPosition()
    return FindPlayersInRange(x, y, z, math.max(handle.dimensions.length:value(), handle.dimensions.width:value()))
end

--(H): lots of more work needs to be done on interior entity collection implementation
--in hamlet they go back to a bunch of prefabs to add interior funcs and tags to make em work when sleeping/waking in interiors
--this seems tedious for u guys to do for each little prefab, i would like to see if it's possible to cover everything in this function instead
--ill include some stuff like "FX" tag but that might be unreliable, idk
--i noticed a lot of stuff when returning to interior needed to turn off/on light, can we handle that in our load and unload automatically? probably
local INTERIOR_CANT_TAGS = {"INTERIOR_LIMBO", "interior_spawn_storage", "interior_handle"}--, "FX"}
--(NOTE, H): uhh nope?? including "FX" breaks stuff and prevents you from interacting with objects? lololol??
function self:FindEntitiesInInterior(handle, musttags, canttags, mustoneoftags)
    local x, y, z = handle.Transform:GetWorldPosition()
    return sim:FindEntities(x, y, z, math.max(handle.dimensions.length:value(), handle.dimensions.width:value()), musttags, canttags or INTERIOR_CANT_TAGS, mustoneoftags)
end

function self:GetSpawnWorldPosition()
    return _interior_spawn_origin:Get()
end

function self:GetStorageWorldPosition()
    return _interior_storage_origin:Get()
end

function self:GetSpawnPosition()
    return Vector3(self:GetSpawnWorldPosition())
end

function self:GetStoragePosition()
    return Vector3(self:GetStorageWorldPosition())
end

function self:GetMaxSize()
    return MAX_INTERIOR_SIZE
end

function self:IsValid()
    return _interior_spawn_origin ~= nil
end

--------------------------------------------------------------------------
--[[ Update ]]
--------------------------------------------------------------------------

function self:Update(dt)
    if not _isdedicated then
        for interior_handle, _ in pairs(_handlers) do
            interior_handle:UpdateParticles(dt)
        end
    end
end

--------------------------------------------------------------------------
--[[ Private event handlers ]]
--------------------------------------------------------------------------

--------------------------------------------------------------------------
--[[ Initialization ]]
--------------------------------------------------------------------------

local function Initialize()
    if _interior_spawn_origin then return end
    
    local w, h = _map:GetSize()
    local world_width, world_height = w * TILE_SCALE, h * TILE_SCALE

    -- tmp
    _interior_spawn_origin = Vector3(world_width + MAX_INTERIOR_SIZE, 0, 0)
    _interior_storage_origin = Vector3(0, 0, world_height + MAX_INTERIOR_SIZE)

    DIMENSION_DEFS.Initialize()

    Map:InitInteriorManager(self)
end

_world:ListenForEvent("worldmapsetsize", Initialize)
    
end)

return InteriorManager