GLOBAL.setfenv(1, GLOBAL)
--

local _minimap_to_data = {} --[inst.MiniMapEntity] = {inst = inst, icon = icon, priority = priority, iconentity = interiorminimapicon}


--

local _AddMiniMapEntity = Entity.AddMiniMapEntity
function Entity:AddMiniMapEntity(...)
	local guid = self:GetGUID()
	local inst = Ents[guid]
    if not inst or inst.MiniMapEntity then return _AddMiniMapEntity(self, ...) end

	local minimap_entity = _AddMiniMapEntity(self, ...)
	_minimap_to_data[minimap_entity] = {inst = inst, priority = 0}

	return minimap_entity
end

--TODO (H): These must be networked if called on the server, add a flag to allow networking after SetPristine is called on master.
-- Hmm I wonder if maybe a custom "inactive netvar" that gets activated (turned into a real one) when SetPristine is called would work? -Half
local _SetIcon = MiniMapEntity.SetIcon
function MiniMapEntity:SetIcon(icon, ...)
    local minimap_data = _minimap_to_data[self]
    if minimap_data ~= nil then 
        minimap_data.icon = icon
        if minimap_data.iconentity ~= nil then
            minimap_data.iconentity.icon:set(icon)
        end 
    end
    return _SetIcon(self, icon, ...)
end

local _SetDrawOverFogOfWar = MiniMapEntity.SetDrawOverFogOfWar
function MiniMapEntity:SetDrawOverFogOfWar(enable, ...)
    local minimap_data = _minimap_to_data[self]
    if minimap_data then 
        minimap_data.fog_immune = enable
        if minimap_data.iconentity ~= nil then
            minimap_data.iconentity.fog_immune:set(enable)
        end 
    end
    return _SetDrawOverFogOfWar(self, enable, ...)
end

function MiniMapEntity:CanDrawOverFogOfWar(enable, ...)
    local minimap_data = _minimap_to_data[self]
    return minimap_data and minimap_data.fog_immune or false
end

-- TODO: Needs to be updated -Half
-- local _SetIsFogRevealer = MiniMapEntity.SetIsFogRevealer
-- function MiniMapEntity:SetIsFogRevealer(enable, ...)

-- end

local _SetPriority = MiniMapEntity.SetPriority
function MiniMapEntity:SetPriority(priority, ...)
    local minimap_data = _minimap_to_data[self]
    if minimap_data then 
        minimap_data.priority = priority
        if minimap_data.iconentity ~= nil then
            minimap_data.iconentity.priority:set(priority)
        end 
    end
    return _SetPriority(self, priority, ...)
end

function MiniMapEntity:GetIcon()
    local minimap_data = _minimap_to_data[self]
	return minimap_data and minimap_data.icon or "NO_ICON"
end

function MiniMapEntity:GetPriority()
    local minimap_data = _minimap_to_data[self]
	return minimap_data and minimap_data.priority or 0
end

-- TODO: ID or bool? -Half
function MiniMapEntity:_SetInteriorMode(enable)
    local minimap_data = _minimap_to_data[self]
	if enable then
        if minimap_data.iconentity == nil then
            minimap_data.iconentity = SpawnPrefab("interior_minimapentity_classified")

            minimap_data.iconentity.priority:set(minimap_data.priority or 0)
            minimap_data.iconentity.fog_immune:set(minimap_data.fog_immune or false)
            minimap_data.iconentity.icon:set(minimap_data.icon or "")

            minimap_data.iconentity.entity:SetParent(minimap_data.inst.entity)
        end
    else
        if minimap_data.iconentity ~= nil then
            minimap_data.iconentity:Remove()
            minimap_data.iconentity = nil
        end
    end
end

function MiniMapEntity:InteriorCore_OnDestroy()
    local minimap_data = _minimap_to_data[self]
    if minimap_data ~= nil then
        if minimap_data.iconentity ~= nil then
            minimap_data.iconentity:Remove()
        end
        _minimap_to_data[self] = nil
    end
	--TODO(H): might need to add something here in the future
end
