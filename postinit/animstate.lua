local Debug_Util = Debug_Util
GLOBAL.setfenv(1, GLOBAL)

local unpack = unpack
local assert = assert

local _rotating_billboards = {}

local RB_SHADER = resolvefilepath("shaders/rotating_billboard.ksh")

local _SetOrientation = AnimState.SetOrientation
function AnimState:SetOrientation(orientation, ...)
    if orientation == ANIM_ORIENTATION.RotatingBillBoard then
        _rotating_billboards[self] = true
        self:SetScale(1, 0.95, 1) -- Can't seem to get the scale to work in the shader so I am applying outside -Half
        self:SetDefaultEffectHandle(RB_SHADER)
        orientation = ANIM_ORIENTATION.BillBoard
    elseif _rotating_billboards[self] ~= nil then
        self:SetScale(1, 1, 1)
        self:ClearDefaultEffectHandle()
    end
    return _SetOrientation(self, orientation, ...)
end

function AnimState:InteriorCore_OnDestroy()
    if _rotating_billboards[self] ~= nil then
        _rotating_billboards[self] = nil
    end
end
