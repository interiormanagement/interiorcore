local Debug_Util = Debug_Util
GLOBAL.setfenv(1, GLOBAL)

local unpack = unpack
local assert = assert
local Ents = Ents

function SilenceEvent(event, data, ...)
    return event .. "_silenced", data
end

if EntityScript.AddPushEventPostFn == nil then
    function EntityScript:AddPushEventPostFn(event, fn, source)
        source = source or self

        if not source.pushevent_postfn then
            source.pushevent_postfn = {}
        end

        source.pushevent_postfn[event] = fn
    end

    local _PushEvent = EntityScript.PushEvent
    function EntityScript:PushEvent(event, data, ...)
        local eventfn = self.pushevent_postfn and self.pushevent_postfn[event] or nil

        if eventfn then
            local newevent, newdata = eventfn(event, data, ...)

            if newevent then
                event = newevent
            end
            if newdata then
                data = newdata
            end
        end

        _PushEvent(self, event, data, ...)
    end
end

if EntityScript.GetEventCallbacks == nil then
    function EntityScript:GetEventCallbacks(event, source, source_file)
        source = source or self

        assert(self.event_listening[event] and self.event_listening[event][source])

        for _, fn in ipairs(self.event_listening[event][source]) do
            if source_file then
                local info = debug.getinfo(fn, "S")
                if info and info.source == source_file then
                    return fn
                end
            else
                return fn
            end
        end
    end
end

-- Note: Despite this being an Entity function, it belongs with EntityScript
-- AddNetworkProxy allows us to skip uneeded netvars and increase preformance -Half
if Entity.AddNetworkProxy == nil then
    local AddNetworkProxy = Debug_Util.GetUpvalue(Entity.AddNetwork, "AddNetworkProxy")
    if AddNetworkProxy == nil then
        AddNetworkProxy = Entity.AddNetwork
        print("Warning: NetworkProxy not found, expect minimap to cause some lag")
    end

    Entity.AddNetworkProxy = AddNetworkProxy
end

-- for houses
function EntityScript:GetInteriorID()
    return self.entity:GetInteriorID()
end

function EntityScript:SetInteriorID(id)
    assert(id == nil or id > 0, "Tried setting invalid interior id", id, " for", self)

    if not self.interior_label then -- debug
        local label = self.entity:AddLabel()
        self.interior_label = label
        label:SetColour(unpack(PLAYERCOLOURS.CORAL))
        label:SetWorldOffset(0, 1, 0)
        label:SetFont(CHATFONT_OUTLINE)
        label:SetFontSize(16)
        label:Enable(true)
    end
    self.interior_label:SetText(tostring(id or 0)) 

    if self.children then
        for k,v in pairs(self.children) do
            k:SetInteriorID(id)
        end
    end

    self.entity:SetInteriorID(id)
end

local _Remove = EntityScript.Remove
function EntityScript:Remove(...)
	if self.MiniMapEntity then
		self.MiniMapEntity:InteriorCore_OnDestroy()
	end
	if self.LightWatcher then
		self.LightWatcher:InteriorCore_OnDestroy()
	end
    if self.entity then
		self.entity:InteriorCore_OnDestroy()
	end
	return _Remove(self, ...)
end
