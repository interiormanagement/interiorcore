GLOBAL.setfenv(1, GLOBAL)

------------------House Patches-------------
local TILE_SCALE = TILE_SCALE
local math = math

function Map:InitInteriorManager(interior_manager)
    local _GetTile = Map.GetTile
    function Map:GetTile(tx, ty, ...)
        local w, h = self:GetSize()
        local x, y, z = (tx - w/2.0)*TILE_SCALE, 0, (ty - h/2.0)*TILE_SCALE
        local interior = interior_manager:GetInteriorHandleAtPoint(x, z)
        if interior then
            -- TODO: This sucks, just pass the id -Half
            return WORLD_TILES[interior.data.tile:value()]
        end

        return _GetTile(self, tx, ty, ...)
    end

    local _GetTileAtPoint = Map.GetTileAtPoint
    function Map:GetTileAtPoint(x, y, z, ...)
        local interior = interior_manager:GetInteriorHandleAtPoint(x, z)
        if interior then
            -- TODO: This sucks, just pass the id -Half
            return WORLD_TILES[interior.data.tile:value()]
        end

        return _GetTileAtPoint(self, x, y, z, ...)
    end

    local _IsVisualGroundAtPoint = Map.IsVisualGroundAtPoint
    function Map:IsVisualGroundAtPoint(x, y, z, ...)
        if interior_manager:GetInteriorHandleAtPoint(x, z) then
            return true
        end

        return _IsVisualGroundAtPoint(self, x, y, z, ...)
    end
    --(H): GetTileCenterPoint breaks outside of the map in vanilla engine so, heres a fix
    local _GetTileCenterPoint = Map.GetTileCenterPoint
    function Map:GetTileCenterPoint(x, y, z, ...)
        z = z or y
        if interior_manager:IsPointInInteriorSpawn(x, z) then
            return math.floor(x/TILE_SCALE)*TILE_SCALE + 2, 0, math.floor(z/TILE_SCALE)*TILE_SCALE + 2
        end
        
        return _GetTileCenterPoint(self, x, 0, z, ...)
    end

    Map.InitInteriorManager = nil
end
--------------------------------------------