GLOBAL.setfenv(1, GLOBAL)

local _c_goto = c_goto
function c_goto(dest, inst, ...)
    if type(dest) == "string" or type(dest) == "number" then
        dest = UserToPlayer(dest)
    end
    if dest then
        inst = ListingOrConsolePlayer(inst)
        if inst then
            local _world = TheWorld
            local _interiorspawner = _world and _world.components.interiorspawner

            local dest_interior_id = dest:GetInteriorID()
            local source_interior_id = inst:GetInteriorID()
            if _interiorspawner and dest_interior_id ~= source_interior_id then
                if source_interior_id then
                    -- remove us from the source room
                    _interiorspawner:ExitInterior(self, source_interior_id, dest)
                end
                if dest_interior_id then
                    -- add us to the dest room
                    _interiorspawner:EnterInterior(self, dest_interior_id, dest)
                end
                SuUsed("c_goto", true)
                return dest
            end
        end
    end
    return _c_goto(dest, inst, ...)
end