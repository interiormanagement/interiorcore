GLOBAL.setfenv(1, GLOBAL)

local Widget = require "widgets/widget"
local Image = require "widgets/image"
local Room = require "widgets/room"

local _atlas_lookup = {} --e.g. ["pond.png"] = "images/minimap_atlas.xml"
local _minimap_atlases = {} --e.g. [1] = "images/atlas.xml"

function GetMiniMapIconAtlas(imagename)
	local atlas = _atlas_lookup[imagename]
	if atlas then
		return atlas
	end

	for i, v in pairs(_minimap_atlases) do
    	atlas = TheSim:AtlasContains(v, imagename) and v or nil
		if atlas then
			_atlas_lookup[imagename] = atlas
			break
		end
	end

	return atlas
end


local _handles = {}
local  _interior_mode = false
local _visible = false

local _interior_visible = false
local _zoom = 1
local _offset_x, _offset_y = 0, 0
local _fogofwar = true

-- Class that manages a single instance of the minimap
-- TODO: Very inefficent, we should try to only update whats changed -Half
local InteriorMiniMapHandle = Class(Widget, function(self)
    Widget._ctor(self, "InteriorMiniMapHandle")
    local inst = self.inst
    self:Hide()
    self._rooms = {}
    self._img = nil

    local _OnRemoveEntity = inst.OnRemoveEntity
    local _self = self
    function inst:OnRemoveEntity(...)
        _self:DetachImage()
        if _OnRemoveEntity then
            _OnRemoveEntity(_self, ...)
        end
    end    
end)

function InteriorMiniMapHandle:_CreateInteriorMap()
    local _player = ThePlayer
    local current_interior = _player.replica.interiorplayer:GetInteriorHandle()

    local mapdata = _player.player_classified.MapExplorer:RecordInteriorMapData()
    local veiwed_group = current_interior and current_interior.data.group:value() -- TODO: Allow "veiwing" other interiors qol
    local current_id = current_interior and current_interior.data.id:value()

    if mapdata then

        --TODO(H): allow group to be set! Qol from hamlet be be allowing players to view other interior group maps
        
        --all of this assumes room texture is 64x64 pixels
        --todo(h): BLENDMODE.Additive needs to be applied to everything, but theres an issue with multiple additive blending widgets overlaying on top of eachother, use shaders....
        --todo(H): order icons properly, augh, MoveToFront and MoveToBack is so silly to use -_-, wish we could just use a simple set sort order and pass in a number
        for k, v in pairs(mapdata[veiwed_group] or {}) do
            local occupied = v.id == current_id

            self._rooms[k] = self:AddChild(Room(v.name, v.length, v.width))
    
            for i, priority_group in pairs(v.priority_order) do
                for i, guid in pairs(v.object_order[priority_group]) do
                    local data = v.object_data[guid]
                    self._rooms[k]:AddMiniMapIcon(data.name, data.x, data.y, data.fog_immune, data.doordirection)
                end
            end

            if not occupied then
                self._rooms[k]:SetFogOfWar(_fogofwar) -- not working? -Half
            end

            self._rooms[k]:SetRoomPosition(v.pos.x, v.pos.y)
        end
    end
end

function InteriorMiniMapHandle:_ClearInteriorMap()
    self:KillAllChildren()
    self._rooms = {}
end

function InteriorMiniMapHandle:_ResetMap()
    self:_ClearInteriorMap()
    self:_CreateInteriorMap()
end

function InteriorMiniMapHandle:Update() -- tmp
    if self.shown then
        self:_ResetMap()
    end
end

function InteriorMiniMapHandle:DetachImage()
    if self.parent ~= nil then
        self.parent:RemoveChild(self)
    end
    for i,v in pairs(_handles) do
        if v == self then
            table.remove(_handles, i)
        end
    end
end

function InteriorMiniMapHandle:AttachImage(img)
    self:DetachImage()
    img:AddChild(self)
    table.insert(_handles, self)
    self:UpdateZoom()
    self:UpdateOffset()
    self:UpdateVisibility()
    return self
end

function InteriorMiniMapHandle:UpdateZoom()
    local z = 1/_zoom
    self:SetScale(z, z)
end

function InteriorMiniMapHandle:UpdateOffset()
    self:SetPosition(_offset_x, _offset_y)
end

function InteriorMiniMapHandle:UpdateVisibility()
    if _interior_visible then
        self:Show()
    else
        self:Hide()
    end
end

function InteriorMiniMapHandle:OnShow(was_hidden)
    if was_hidden then
        self:_CreateInteriorMap()
    end
end

function InteriorMiniMapHandle:OnHide(was_visible)
    if was_visible then
        self:_ClearInteriorMap()
    end
end

local function UpdateHandleZoom()
    for k, v in ipairs(_handles) do
        v:UpdateZoom()
    end
end

local function UpdateHandlePos()
    for k, v in ipairs(_handles) do
        v:UpdateOffset()
    end
end

local function UpdateHandleVisibility()
    for k, v in ipairs(_handles) do
        v:UpdateVisibility()
    end
end

local function UpdateHandles()
    for k, v in ipairs(_handles) do
        v:Update()
    end
end

-- function self:VeiwInterior(interior)
--     _veiwed_interior = interior
-- end

function MiniMap:IsInteriorMode()
    return _interior_mode
end

local _IsVisible = MiniMap.IsVisible
local _ToggleVisibility = MiniMap.ToggleVisibility

function MiniMap:IsVisible(...)
    return _IsVisible(self, ...) or _interior_visible
end

local function EnableMiniMap(self)
    if not _IsVisible(self) then
        _ToggleVisibility(self)
    end
end

local function DisableMiniMap(self)
    if _IsVisible(self) then
        _ToggleVisibility(self)
    end
end

local function EnableInteriorMiniMap()
    _interior_visible = true
    UpdateHandleVisibility()
end

local function DisableInteriorMiniMap()
    _interior_visible = false
    UpdateHandleVisibility()
end

local function UpdateVisibility(self)
    if _visible then
        if _interior_mode then
            DisableMiniMap(self)
            EnableInteriorMiniMap()
        else
            EnableMiniMap(self)
            DisableInteriorMiniMap()
        end
    else
        DisableMiniMap(self)
        DisableInteriorMiniMap()
    end
end

function MiniMap:ToggleVisibility(...)
    _visible = not _visible
    UpdateVisibility(self)
end

function MiniMap:SetInteriorMode(enable)
    _interior_mode = enable
    UpdateVisibility(self)
end

-- local _WorldPosToMapPos = MiniMap.WorldPosToMapPos
-- function MiniMap:WorldPosToMapPos(x, y, z, ...)
--     -- if _interior_mode then
--     --     return
--     -- end
--     return _WorldPosToMapPos(self, x, y, z, ...)
-- end

-- local _MapPosToWorldPos = MiniMap.MapPosToWorldPos
-- function MiniMap:MapPosToWorldPos(x, y, z, ...)
--     -- if _interior_mode then
--     --     return
--     -- end
--     return _MapPosToWorldPos(self, x, y, z, ...)
-- end

local _Zoom = MiniMap.Zoom
function MiniMap:Zoom(delta, ...)
    _zoom = math.clamp(_zoom + delta, MINIMAP.ZOOM_CLAMP_MIN, MINIMAP.ZOOM_CLAMP_MAX)
    UpdateHandleZoom()
    return _Zoom(self, delta, ...)
end

local _GetZoom = MiniMap.GetZoom
function MiniMap:GetZoom(...)
    if _interior_mode then
        return _zoom
    end
    return _GetZoom(self, ...)
end

local _Offset = MiniMap.Offset
function MiniMap:Offset(dx, dy, ...)
    _offset_x, _offset_y = _offset_x + dx * 4, _offset_y + dy * 4
    UpdateHandlePos()
    return _Offset(self, dx, dy, ...)
end

local _ResetOffset = MiniMap.ResetOffset
function MiniMap:ResetOffset(...)
    _offset_x, _offset_y = 0, 0
    UpdateHandlePos()
    return _ResetOffset(self, ...)
end

local _EnableFogOfWar = MiniMap.EnableFogOfWar
function MiniMap:EnableFogOfWar(enable_fog, ...)
    _fogofwar = enable_fog
    UpdateHandles()
    return _EnableFogOfWar(self, enable_fog, ...)
end

function MiniMap:_UpdateInterior(interior_id, interior_group) -- tmp
    UpdateHandles()
end

-- Well this terminology is getting confusing...
-- This is NOT an interior_handle, it's an interior minimap handle -Half
function MiniMap:GetInteriorHandle()
    return InteriorMiniMapHandle
end

local _AddAtlas = MiniMap.AddAtlas
function MiniMap:AddAtlas(atlas, ...)
	if not table.contains(_minimap_atlases, atlas) then table.insert(_minimap_atlases, atlas) end
	return _AddAtlas(self, atlas, ...)
end
