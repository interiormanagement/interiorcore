local Debug_Util = Debug_Util
GLOBAL.setfenv(1, GLOBAL)

local unpack = unpack
local assert = assert
local Ents = Ents

local _interior_id = {}

function Entity:SetInteriorID(id)
    local guid = self:GetGUID()
    local inst = Ents[guid]
    if inst.MiniMapEntity then
        inst.MiniMapEntity:_SetInteriorMode(id ~= nil)
    end
    _interior_id[self] = id
end

function Entity:GetInteriorID(id)
    return _interior_id[self]
end

function Entity:InteriorCore_OnDestroy()
    if _interior_id[self] ~= nil then
        _interior_id[self] = nil
    end
end