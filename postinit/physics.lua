GLOBAL.setfenv(1, GLOBAL)

local function RectangleMesh(vertices, height)
    local triangles = {}
    local y0 = 0
    local y1 = height
 
    local idx0 = #vertices
    for idx1 = 1, #vertices do
        local x0, z0 = vertices[idx0].x, vertices[idx0].z
        local x1, z1 = vertices[idx1].x, vertices[idx1].z
 
        table.insert(triangles, x0)
        table.insert(triangles, y0)
        table.insert(triangles, z0)
 
        table.insert(triangles, x0)
        table.insert(triangles, y1)
        table.insert(triangles, z0)
 
        table.insert(triangles, x1)
        table.insert(triangles, y0)
        table.insert(triangles, z1)
 
        table.insert(triangles, x1)
        table.insert(triangles, y0)
        table.insert(triangles, z1)
 
        table.insert(triangles, x0)
        table.insert(triangles, y1)
        table.insert(triangles, z0)
 
        table.insert(triangles, x1)
        table.insert(triangles, y1)
        table.insert(triangles, z1)
 
        idx0 = idx1
    end
 
    return triangles
end

function Physics:SetRectangle(length, height, width) -- x, y, z
    local halfwidth = width/2
    local halflength = length/2
    local vertexes = {
        Vector3(halfwidth, 0, -halflength),
        Vector3(-halfwidth, 0, -halflength),
        Vector3(-halfwidth, 0, halflength),
        Vector3(halfwidth, 0, halflength),
    }
    self:SetTriangleMesh(RectangleMesh(vertexes, height))
end