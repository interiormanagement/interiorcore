local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

IAENV.AddComponentPostInit("ambientsound", function(cmp)
    local inst = cmp.inst

    local _reverb_override = nil
    local _old_reverb = nil

    local _SetReverbPreset = cmp.SetReverbPreset
    function cmp:SetReverbPreset(reverb, ...)
        if not _reverb_override then
            _SetReverbPreset(self, reverb, ...)
        end		
        _old_reverb = reverb
    end
    
    function cmp:SetReverbOverride(reverb)
        _reverb_override = reverb
        TheSim:SetReverbPreset(reverb)
    end
    
    function cmp:ClearReverbOverride()
        _reverb_override = nil	
        TheSim:SetReverbPreset(_old_reverb or "default")
    end
end)
