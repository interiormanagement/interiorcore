GLOBAL.setfenv(1, GLOBAL)

local Moisture = require("components/moisture")

local _GetMoistureRate = Moisture.GetMoistureRate
function Moisture:GetMoistureRate(...)
    -- TODO interior support
    return _GetMoistureRate(self, ...)
end
