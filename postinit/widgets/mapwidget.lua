GLOBAL.setfenv(1, GLOBAL)

local unpack = unpack

local MapWidget = require "widgets/mapwidget"
local InteriorMiniMapImage = require "widgets/interiorminimapimage"

local __ctor = MapWidget._ctor
function MapWidget:_ctor(...)
    __ctor(self, ...)

    self.int_img = self:AddChild(InteriorMiniMapImage())
    self.int_img:SetHAnchor(ANCHOR_MIDDLE)
    self.int_img:SetVAnchor(ANCHOR_MIDDLE)
    self.int_img:SetClickable(false)
    self.int_img:MoveToBack()
    self.int_img:Hide()

    self.int_blackbg = self:AddChild(Image("images/global.xml", "square.tex"))
    self.int_blackbg:SetVRegPoint(ANCHOR_MIDDLE)
    self.int_blackbg:SetHRegPoint(ANCHOR_MIDDLE)
    self.int_blackbg:SetVAnchor(ANCHOR_MIDDLE)
    self.int_blackbg:SetHAnchor(ANCHOR_MIDDLE)
    self.int_blackbg:SetScaleMode(SCALEMODE_FILLSCREEN)
    self.int_blackbg:SetTint(0,0,0,1)
	self.int_blackbg:SetClickable(false)
    self.int_blackbg:MoveToBack()
    self.int_blackbg:Hide()
end

local _UpdateTexture = MapWidget.UpdateTexture
function MapWidget:UpdateTexture(...)
    local rets = {_UpdateTexture(self, ...)}
    local handle = self.minimap:GetInteriorHandle()
    self.int_img:SetInteriorHandle( handle )
    self:UpdateInteriorMode() --tmp ig?
    return unpack(rets)
end

function MapWidget:UpdateInteriorMode()
    if self.minimap:IsInteriorMode() then
        self.bg.inst.ImageWidget:SetBlendMode( BLENDMODE.Additive )

        self.bg:SetClickable(true)
        self.img:SetClickable(false)

        self.img:Hide()
        self.int_img:Show()
        self.int_blackbg:Show() 
    else
        self.bg.inst.ImageWidget:SetBlendMode( BLENDMODE.Premultiplied )

        self.bg:SetClickable(false)
        self.img:SetClickable(true)

        self.img:Show()
        self.int_img:Hide()
        self.int_blackbg:Hide()
    end
end

-- Compat with the minimap mod
-- TODO: Make sure this is THE ORIGINAL minimap mod, theres a bunch with the same file path... -Half
if not softresolvefilepath("scripts/widgets/minimapwidget.lua") then
    return
end

local MiniMapWidget = require "widgets/minimapwidget"

local __ctor = MiniMapWidget._ctor
function MiniMapWidget:_ctor(...)
    -- Well this is cursed

    self.int_img = InteriorMiniMapImage()
    self.int_blackbg = Image("images/global.xml", "square.tex")

    __ctor(self, ...)

    if not self.inst:IsValid() then
        self.int_img:Kill()
        self.int_blackbg:Kill()
        return
    end
    
    self.int_img = self:AddChild(self.int_img)
    self.int_img:SetSize(self.mapsize.w, self.mapsize.h, 0)
    self.int_img:SetClickable(false)
    self.int_img:MoveToBack()
    self.int_img:Hide()

    self.int_blackbg = self:AddChild(self.int_blackbg)
    self.int_blackbg:SetSize(self.mapsize.w, self.mapsize.h, 0)
    self.int_blackbg:SetTint(0,0,0,0.75)
	self.int_blackbg:SetClickable(false)
    self.int_blackbg:MoveToBack()
    self.int_blackbg:Hide()
    local _SetSize = self.img.SetSize
    self.img.SetSize = function(_self, w,h, ...)
        _SetSize(_self, w,h, ...)
        self.int_img:SetSize(w,h, 0)
    end

    self:UpdateInteriorMode()
end

function MiniMapWidget:UpdateInteriorMode()
    if self.minimap:IsInteriorMode() then
        self.bg.inst.ImageWidget:SetBlendMode( BLENDMODE.Additive )

        self.bg:SetClickable(true)
        self.img:SetClickable(false)

        if self.open then
            self.img:Hide()
            self.int_img:Show()
            self.int_blackbg:Show() 
        end
    else
        self.bg.inst.ImageWidget:SetBlendMode( BLENDMODE.Premultiplied )

        self.bg:SetClickable(false)
        self.img:SetClickable(true)

        if self.open then
            self.img:Show()
            self.int_img:Hide()
            self.int_blackbg:Hide()
        end
    end
end


local _SetOpen = MiniMapWidget.SetOpen
function MiniMapWidget:SetOpen(state, ...)
    local rets = {_SetOpen(self, state, ...)}
	if state == nil then state = true end

	if state then
    	self.int_blackbg:SetPosition( 0,0,0 )
    	self.int_blackbg:SetSize( self.mapsize.w, self.mapsize.h )
	else
    	self.int_blackbg:SetPosition( 0, self.mapsize.h/2 - 20, 0 )
    	self.int_blackbg:SetSize( self.mapsize.w, 5 )
	end
    self:UpdateInteriorMode()
    return unpack(rets)
end

local _OnGainFocus = MiniMapWidget.OnGainFocus
function MiniMapWidget:OnGainFocus(...)
	self.int_blackbg:SetTint(0,0,0,1)
    return _OnGainFocus(self, ...)
end

local _OnLoseFocus = MiniMapWidget.OnLoseFocus
function MiniMapWidget:OnLoseFocus(...)
	self.int_blackbg:SetTint(0,0,0,0.75)
    return _OnLoseFocus(self, ...)
end

local _UpdateTexture = MapWidget.UpdateTexture
function MiniMapWidget:UpdateTexture(...)
    local rets = {_UpdateTexture(self, ...)}
    local handle = self.minimap:GetInteriorHandle()
    self.int_img:SetInteriorHandle( handle )
    self:UpdateInteriorMode() --tmp ig?
    return unpack(rets)
end

local _OnZoomIn = MiniMapWidget.OnZoomIn
function MiniMapWidget:OnZoomIn(...)
    local rets = {_OnZoomIn(self, ...)}
    if self.shown then
        self.img:SetUVScale(self.uvscale, self.uvscale)
    end
    return unpack(rets)
end

local _OnZoomOut = MiniMapWidget.OnZoomOut
function MiniMapWidget:OnZoomOut(...)
    local _minimapzoom = self.minimapzoom
    local rets = {_OnZoomOut(self, ...)}
    if self.shown then
        if _minimapzoom == 0 then
            self.img:SetUVScale(self.uvscale, self.uvscale)
        end
    end
    return unpack(rets)
end
-- end

local _OnUpdate = MiniMapWidget.OnUpdate
function MiniMapWidget:OnUpdate(...)
    _OnUpdate(self, ...)
	if not self.shown then return end

	if TheInput:IsControlPressed(CONTROL_PRIMARY) then
		local pos = TheInput:GetScreenPosition()
		if self.lastpos then
			local scale = 2 / 9
			local dx = scale * ( pos.x - self.lastpos.x )
			local dy = scale * ( pos.y - self.lastpos.y )
			self.minimap:Offset( dx, dy )
		end

		self.lastpos = pos
	else
		self.lastpos = nil
	end
end
