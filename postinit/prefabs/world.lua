local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local Interior

IAENV.AddPrefabPostInit("world", function(inst)
    InteriorManager = require("interiormanager")(inst)

    if not inst.ismastersim then
        return
    end

    inst:AddComponent("interiorspawner")
end)