GLOBAL.setfenv(1, GLOBAL)

local light_at_dist = light_at_dist

--(H): Only necessary for server, we override it to be dark on client side, but server needs support for exclusive areas of darkness.

if not TheNet:GetIsMasterSimulation()  then
	function LightWatcher:InteriorCore_OnDestroy() end
	return 
end

--

local _lightwatcher_to_data = {} --[inst.LightWatcher] = {inst = inst, darkthresh = 0, lightthresh = 0, minlightthresh = 0, inlight = true, timedark = 0, timelight = 0}
local _lightwatchers_to_update = {} --[inst.LightWatcher] = true

--

local _Update = Update
function Update(dt, ...)
	-- print("Test, Update is called")
	for lightwatcher, v in pairs(_lightwatchers_to_update) do
		--(h): pause if unloaded??
		local watcher_data = _lightwatcher_to_data[lightwatcher]
		if watcher_data.inst:GetInteriorID() then
			--print(TheWorld.ismastersim, "This is the mastersim", k:IsInLight())
			if not watcher_data.inlight and lightwatcher:IsInLight() then
				watcher_data.inlight = true
				watcher_data.timelight = GetTime()
				watcher_data.timedark = nil
				watcher_data.inst:PushEvent("enterlight")
			elseif watcher_data.inlight and not lightwatcher:IsInLight() then
				watcher_data.inlight = false
				watcher_data.timedark = GetTime()
				watcher_data.timelight = nil
				watcher_data.inst:PushEvent("enterdark")
			end
		end
	end
	--
	_Update(dt, ...)
end

local _ListenForEvent = EntityScript.ListenForEvent
function EntityScript:ListenForEvent(event, fn, source, ...)
	if event == "enterdark" or event == "enterlight" then
		assert(self.LightWatcher, "Tried to listen for enterdark/enterlight event but this entity has no LightWatcher!")
        if _lightwatchers_to_update[self.LightWatcher] == nil then
            _lightwatchers_to_update[self.LightWatcher] = 0
        end
		_lightwatchers_to_update[self.LightWatcher] = _lightwatchers_to_update[self.LightWatcher] + 1
	end
	return _ListenForEvent(self, event, fn, source, ...)
end

local _RemoveEventCallback = EntityScript.RemoveEventCallback
function EntityScript:RemoveEventCallback(event, fn, source, ...)
	if event == "enterdark" or event == "enterlight" then
		_lightwatchers_to_update[self.LightWatcher] = _lightwatchers_to_update[self.LightWatcher] - 1
        if _lightwatchers_to_update[self.LightWatcher] == 0 then
            _lightwatchers_to_update[self.LightWatcher] = nil

            local watcher_data = _lightwatcher_to_data[self.LightWatcher]
            watcher_data.inlight = nil
            watcher_data.timelight = nil
            watcher_data.timedark = nil
        end
	end
	return _RemoveEventCallback(self, event, fn, source, ...)
end

local _AddLightWatcher = Entity.AddLightWatcher
function Entity:AddLightWatcher(...)
	local guid = self:GetGUID()
	local inst = Ents[guid]
    if not inst or inst.LightWatcher then return _AddLightWatcher(self, ...) end

	local lightwatcher = _AddLightWatcher(self, ...)
    _lightwatcher_to_data[lightwatcher] = {inst = inst}

	return lightwatcher
end

local _SetDarkThresh = LightWatcher.SetDarkThresh
function LightWatcher:SetDarkThresh(threshold, ...)
    local watcher_data = _lightwatcher_to_data[self]
    if watcher_data == nil then return _SetDarkThresh(self, threshold, ...) end
    
    _lightwatcher_to_data[self].darkthresh = threshold
    return _SetDarkThresh(self, threshold, ...)
end

local _SetLightThresh = LightWatcher.SetLightThresh
function LightWatcher:SetLightThresh(threshold, ...)
    local watcher_data = _lightwatcher_to_data[self]
    if watcher_data == nil then return _SetLightThresh(self, threshold, ...) end

    _lightwatcher_to_data[self].lightthresh = threshold
    return _SetLightThresh(self, threshold, ...)
end


local _SetMinLightThresh = LightWatcher.SetMinLightThresh
function LightWatcher:SetMinLightThresh(threshold, ...)
    local watcher_data = _lightwatcher_to_data[self]
    if watcher_data == nil then return _SetMinLightThresh(self, threshold, ...) end

    _lightwatcher_to_data[self].minlightthresh = threshold
    return _SetMinLightThresh(self, threshold, ...)
end

local _GetLightValue = LightWatcher.GetLightValue
function LightWatcher:GetLightValue(...) --(H) Override lighting in interiors, interiors are dark by default in ambient lighting
    local watcher_data = _lightwatcher_to_data[self]
    if watcher_data == nil then return _GetLightValue(self, ...) end

	local inst = _lightwatcher_to_data[self].inst
	local interior_id = inst and inst:GetInteriorID()
	if interior_id then
		return TheSim:GetLightAtPoint(inst.Transform:GetWorldPosition())
	end

	return _GetLightValue(self, ...)
end

local _GetTimeInDark = LightWatcher.GetTimeInDark
function LightWatcher:GetTimeInDark(...)
	local watcher_data = _lightwatcher_to_data[self]
    if watcher_data == nil then return _GetTimeInDark(self, ...) end

	if watcher_data.timedark then
		return GetTime() - watcher_data.timedark
	elseif watcher_data.timelight then
		return 0
	end

	return _GetTimeInDark(self, ...)
end

local _GetTimeInLight = LightWatcher.GetTimeInLight
function LightWatcher:GetTimeInLight(...)
	local watcher_data = _lightwatcher_to_data[self]
    if watcher_data == nil then return _GetTimeInLight(self, ...) end

	if watcher_data.timelight then
		return GetTime() - watcher_data.timelight
	elseif watcher_data.timedark then
		return 0
	end

	return _GetTimeInLight(self, ...)
end

local _IsInLight = LightWatcher.IsInLight
function LightWatcher:IsInLight(...)
    local watcher_data = _lightwatcher_to_data[self]
    if watcher_data == nil then return _IsInLight(self, ...) end

	local inst = _lightwatcher_to_data[self].inst
	local interior_id = inst and inst:GetInteriorID()
	if interior_id then
		--TODO, over or over AND equal to????? CHECK!
		return self:GetLightValue() > _lightwatcher_to_data[self].darkthresh
	end
	return _IsInLight(self, ...)
end

function LightWatcher:InteriorCore_OnDestroy()
	_lightwatcher_to_data[self] = nil
	_lightwatchers_to_update[self] = nil
	--TODO(H): might need to add something here in the future
end

local _GetLightAtPoint = Sim.GetLightAtPoint
function Sim:GetLightAtPoint(x, y, z, ...)
    local interior_handle = InteriorManager:GetInteriorHandleAtPoint(x, z)
    --print(inside, interior_id)
    if interior_handle then
        local lights = InteriorManager:FindEntitiesInInterior(interior_handle)
        local sum = 0
        for k, v in pairs(lights) do
            if v.Light then
                sum = sum + light_at_dist(v.Light, math.sqrt(v:GetDistanceSqToPoint(x, y, z)))
            end
        end
        --calculations aren't fully accurate, idk why
        --print("GetLightAtPoint interior(server) override( LUA: "..sum.." , C++: ".._GetLightAtPoint(self, x, y, z, ...)..")")
        return sum
    end

	return _GetLightAtPoint(self, x, y, z, ...)
end