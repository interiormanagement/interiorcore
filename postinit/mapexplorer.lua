GLOBAL.setfenv(1, GLOBAL)

local _mapexplorer_to_data = {} --[inst.MapExplorer] = {inst = inst, intmapdata = {}}
local _mapexplorers_to_update = {} --[inst.LightWatcher] = true

local _AddMapExplorer = Entity.AddMapExplorer
function Entity:AddMapExplorer(...)
    local guid = self:GetGUID()
	local inst = Ents[guid]
    if not inst or inst.MapExplorer ~= nil then return _AddMapExplorer(self, ...) end

	local mapexplorer = _AddMapExplorer(self, ...)
	_mapexplorer_to_data[mapexplorer] = {inst = inst, intmapdata = {}, wants_to_update = true}

	return mapexplorer
end

local _intmapdata_sortfn = function(obj1, obj2)
    return obj1 < obj2
end
--[[
-- TODO: Left or right? Check how table.sort sorts -Half
local _bisect_left = function(tbl, obj)

end

-- This code assumes the data is already sorted
-- For the case that objs are removed/added in ocovigil interiors or the icon is global

local function AddSingleObjectToInteriorMapData(intdata, priority, guid, object_data)
    intdata.object_data[guid] = object_data
    if intdata.object_order[priority] == nil then
        intdata.object_order[priority] = {}
        local priority_index = _bisect_left(intdata.priority_order, priority)
        table.insert(intdata.priority_order, priority_index, priority)
    end
    local order_index = _bisect_left(intdata.object_order[priority], guid)
    table.insert(intdata.object_order[priority], order_index, guid)
    -- TODO: Update interior map if active
end

local function RemoveSingleObjectFromInteriorMapData(intdata, priority, guid)
    intdata.object_data[guid] = nil
    local order_index = _bisect_left(intdata.object_order[priority], guid)
    table.remove(intdata.object_order[priority], order_index)
    if #intdata.object_order[priority] == 0 then
        intdata.object_order[priority] = nil
    end
    -- TODO: Update interior map if active
end
--]]

-- TODO: destroy data for discarded groups/interiors
-- function MapExplorer:OnInteriorDestroyed(group, group_x, group_y)

-- end

local INTERIOR_MAP_ICON_TAGS = {"CLASSIFIED", "interior_minimapentity_classified"}
--todo(H): there has to be support for recording interiors we CANT see, passed from server to client, for stuff lik ocuvigils
--also we only update when we first open the map/leaving/entering interiors, we should also update while on the screen like game does with vanilla map
--might need to rework how interiormapwidget is constructed to make for an optimized moving of objects when updating
function MapExplorer:RevealInterior(interior_handle)
	--[[
	(H): When called updates map data of current interior and stores it for map widget to use
	--should be called on client ONLY
	]]
    local mapexplorer_data = _mapexplorer_to_data[self]
    if mapexplorer_data == nil then return end
    local intmapdata = mapexplorer_data.intmapdata
    
    local group = interior_handle.data.group:value()
    local group_x, group_y = interior_handle.data.group_x:value(), interior_handle.data.group_y:value()
	if intmapdata[group] == nil then intmapdata[group] = {} end

    --clear past tables oops, clean this code up
	for k, v in pairs(intmapdata[group]) do
		if v.pos.x == group_x and v.pos.y == group_y then
			table.remove(intmapdata[group], k)
		end
	end

    local length, width = interior_handle.dimensions.length:value(), interior_handle.dimensions.width:value()
	local botleftpos = interior_handle:GetPosition() + Vector3(width/2, 0, -length/2)

    local object_data = {}
    local priority_order = {}
    local object_order = {}
	for k, v in pairs(InteriorManager:FindEntitiesInInterior(interior_handle, INTERIOR_MAP_ICON_TAGS)) do
        if v ~= nil and v:IsValid() and v.icon:value() ~= "" then
			--determine x and y
			local x, y, z = v.Transform:GetWorldPosition()
			local percy, percx = math.abs(botleftpos.x- x) / width, math.abs(botleftpos.z - z) / length
			--TODO JUST FOR TESTING
			local direction
			-- if v:HasTag("door_north") then
			-- 	direction = "north"
			-- elseif v:HasTag("door_south") then
			-- 	direction = "south"
			-- elseif v:HasTag("door_west") then
			-- 	direction = "west"
			-- elseif v:HasTag("door_east") then
			-- 	direction = "east"
			-- end
            object_data[v.GUID] = {
				name = v.icon:value(),
                fog_immune = v.fog_immune:value(),
				x = percx, --x and y are percents of the rooms width and length
				y = percy,
				doordirection = direction, --TODO, JUST FOR TESTING
			}
            local priority = v.priority:value()
            if object_order[priority] == nil then
                object_order[priority] = {}
                table.insert(priority_order, priority)
            end
            table.insert(object_order[priority], v.GUID)
		end
	end

    table.sort(priority_order, _intmapdata_sortfn)
    for priority_group, objects in pairs(object_order) do
        table.sort(objects, _intmapdata_sortfn) -- Fix z order fighting issues
    end

	table.insert(intmapdata[interior_handle.data.group:value()], {
		name = interior_handle.textures.minimap:value(),
		id = interior_handle.data.id:value(),
		length = length,
		width = width,
		--occupied = false,
		pos = {x = group_x, y = group_y},
		object_data = object_data,
        priority_order = priority_order,
        object_order = object_order,
	})
    -- TODO: Call interior map update
end

function MapExplorer:RecordInteriorMapData()
    local mapexplorer_data = _mapexplorer_to_data[self]
	return mapexplorer_data and mapexplorer_data.intmapdata or nil
end

-- TODO: Allow maprevealers to record and teach interior maps -Half
-- function MapExplorer:RecordMap()

-- end

-- function MapExplorer:LearnRecordedMap(data)

-- end

-- function MapExplorer:IsTileSeeable(tx, ty)

-- end

-- Hmm this wont work properly for unloaded interiors which makes it pointless...
-- function MapExplorer:RevealArea(x, y, z)
--     local interior_handle = InteriorManager:GetInteriorHandleAtPoint(x, z)
--     self:RecordInteriorMapData(interior_handle)
-- end

--

local _Update = Update
function Update(dt, ...)
    local _world = TheWorld
    if _world and _world.minimap.MiniMap then
        for mapexplorer, v in pairs(_mapexplorers_to_update) do
            local mapexplorer_data = _mapexplorer_to_data[mapexplorer]
            mapexplorer:RevealInterior(mapexplorer_data.current_interior)
            _world.minimap.MiniMap:_UpdateInterior()
        end
    end

	_Update(dt, ...)
end

local function MapExplorer_EnableUpdate(self, update)
    if update then
        _mapexplorers_to_update[self] = true
    else
        _mapexplorers_to_update[self] = nil
    end
end

local _EnableUpdate = MapExplorer.EnableUpdate
function MapExplorer:EnableUpdate(enable, ...)
    local mapexplorer_data = _mapexplorer_to_data[self]
    if mapexplorer_data == nil then return _EnableUpdate(self, enable, ...) end
    mapexplorer_data.wants_to_update = enable
    MapExplorer_EnableUpdate(self, mapexplorer_data.wants_to_update and mapexplorer_data.current_interior ~= nil)
    return _EnableUpdate(self, enable, ...)
end

function MapExplorer:SetCurrentInterior(interior)
    local mapexplorer_data = _mapexplorer_to_data[self]
    if mapexplorer_data == nil then return end
    mapexplorer_data.current_interior = interior
    MapExplorer_EnableUpdate(self, mapexplorer_data.wants_to_update and mapexplorer_data.current_interior ~= nil)
end

--[[
function MapExplorer:SaveInteriorMapToPersistentString()
	local str = json.encode({mapdata = MAPEXPLORER_TO_DATA[self].intmapdata})
    TheSim:SetPersistentString("interiormapdata", str, false)
end

function MapExplorer:LoadInteriorMapFromPersistentString()
	TheSim:GetPersistentString("interiormapdata", function(load_success, data) 
        if load_success and data then
            local status, invdata = pcall( function() return json.decode(data) end )
            if status and invdata then
                MAPEXPLORER_TO_DATA[self].intmapdata = invdata.mapdata or {}
            else
                print("Failed to load Interior Map Data!", status, invdata)
            end
        end
    end)
end
]]