local modimport = modimport
local IAENV = env
GLOBAL.IAENV = env
GLOBAL.setfenv(1, GLOBAL)

INTERIORCORE_CONFIG = {
    -- Some of these may be treated as client-side, as indicated by the bool
}

-- TODO: Should prob be somewhere else
IAENV.AddSimPostInit(function()
    local Initialize = require("interior_defs/dimension_defs").Initialize
    Initialize()
end)

if not IAENV.MODROOT:find("workshop-") then
	CHEATS_ENABLED = true 
    require("debugkeys")
end


modimport("main/tuning")
modimport("main/constants")
modimport("main/tiledefs")

modimport("main/debug_util")
modimport("main/houseutil")
modimport("main/commands")
modimport("main/standardcomponents")

modimport("main/assets")

modimport("main/RPC")
modimport("main/actions")
modimport("main/postinit")

-- TODO: Not here -Half
local _Update = Update
function Update(dt, ...)
    _Update(dt, ...)
    if InteriorManager then
        InteriorManager:Update(dt)
    end
end

------------------------------ HAM Replicatable Components ------------------------------------------------------------

AddReplicableComponent("interiorplayer")

------------------------------ Replicatable Components ---------------------------------------