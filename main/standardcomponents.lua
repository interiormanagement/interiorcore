GLOBAL.setfenv(1, GLOBAL)

-- Not for interior handler use -Half
function MakeInteriorObjectPhysics(inst, length, height, width)
    length = length or 15
    height = height or 20
    width = width or 10

    inst:AddTag("blocker")
    inst.entity:AddPhysics()
    inst.Physics:SetMass(0) 
    inst.Physics:SetRectangle(length, height, width)
    inst.Physics:SetCollisionGroup(COLLISION.GROUND)
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.ITEMS)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)    
end
