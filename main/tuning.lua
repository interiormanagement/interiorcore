GLOBAL.setfenv(1, GLOBAL)

local tuning = {
    MAX_INTERIOR_SIZE = 50
}

for key, value in pairs(tuning) do
    if TUNING[key] then
        print("OVERRIDE: " .. key .. " in TUNING")
    end

    TUNING[key] = value
end
