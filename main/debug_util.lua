local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local Debug_Util = {}

local hidefns = {}
function Debug_Util.HideHackFn(hidefn, realfn)
    hidefns[hidefn] = realfn
end

local _debug_getupvalue = debug.getupvalue
function debug.getupvalue(fn, ...)
    local rets = {_debug_getupvalue(hidefns[fn] or fn, ...)}
    return unpack(rets)
end
Debug_Util.HideHackFn(debug.getupvalue, _debug_getupvalue)

local _debug_setupvalue = debug.setupvalue
function debug.setupvalue(fn, ...)
    local rets = {_debug_setupvalue(hidefns[fn] or fn, ...)}
    return unpack(rets)
end
Debug_Util.HideHackFn(debug.setupvalue, _debug_setupvalue)

function Debug_Util.GetUpvalue(fn, name, recurse_levels)
    assert(type(fn) == "function")

    recurse_levels = recurse_levels or 0
    local source_fn = fn
    local i = 1

    while true do
        local _name, value = debug.getupvalue(fn, i)
        if _name == nil then
            return
        elseif _name == name then
            return value, i, source_fn
        elseif type(value) == "function" and recurse_levels > 0 then
            local _value, _i, _source_fn = Debug_Util.GetUpvalue(value, name, recurse_levels - 1)
            if _value then
                return _value, _i, _source_fn
            end
        end

        i = i + 1
    end
end

function Debug_Util.SetUpvalue(fn, value, name, recurse_levels)
    local _, i, source_fn = Debug_Util.GetUpvalue(fn, name, recurse_levels)
    debug.setupvalue(source_fn, i, value)
end

IAENV.Debug_Util = Debug_Util