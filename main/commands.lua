GLOBAL.setfenv(1, GLOBAL)

-- house helper commands

function c_createantroom(group, x, y)
	local inst = c_spawn("debug_door")
	local id = CreateAntRoom(group, x, y)
    print("Created ant room with id:", id)

	inst.components.interiordoor.targetInteriorID = id
	--inst.components.interiordoor.targetDoor = id
	return inst
end

function c_createpalaceroom(group, x, y)
	local inst = c_spawn("debug_door")
	local id = CreatePalaceRoom(group, x, y)
    print("Created palace room with id:", id)

	inst.components.interiordoor.targetInteriorID = id
	--inst.components.interiordoor.targetDoor = id
	return inst
end

function c_createruinsroom(group, x, y)
	local inst = c_spawn("debug_door")
	local id = CreateRuinsRoom(group, x, y)
    print("Created ruins room with id:", id)

	inst.components.interiordoor.targetInteriorID = id
	--inst.components.interiordoor.targetDoor = id
	return inst
end

function c_createbatcaveroom(group, x, y)
	local inst = c_spawn("debug_door")
	local id = CreateBatCaveRoom(group, x, y)
    print("Created bat cave room with id:", id)

	inst.components.interiordoor.targetInteriorID = id
	--inst.components.interiordoor.targetDoor = id
	return inst
end

function c_createinteriordoor(group, x, y)
	local inst = c_spawn("debug_door")
	local id = CreateInteriorRoom(group, x, y)
    print("Created interior room with id:", id)

	inst.components.interiordoor.targetInteriorID = id
	--inst.components.interiordoor.targetDoor = id
	return inst
end

function c_createinteriordoorexit(id)
	local inst = c_spawn("debug_door")

	inst.components.interiordoor.targetInteriorID = id
	inst.components.interiordoor.outside = true
	return inst
end

--------------------------------------------------------------------------

local unpack = unpack

--------------------------------------------------------------------------
local ConsoleScreen = require("screens/consolescreen")
local TextEdit = require "widgets/textedit"

local prediction_command = {
    "createantroom", "createpalaceroom", 
    "createruinsroom", "createbatcaveroom", 
    "createinteriordoor", "createinteriordoorexit"
}

local _DoInit = ConsoleScreen.DoInit
function ConsoleScreen:DoInit(...)

    -- Hacky but for some reason I cannot add more commands after the DoInit and cant find out why, neither can Hornet
    local _AddWordPredictionDictionary = TextEdit.AddWordPredictionDictionary
    function TextEdit:AddWordPredictionDictionary(data, ...)
        if data.words and data.delim and data.delim == "c_" then
            for k, v in pairs(prediction_command) do
                table.insert(data.words, v)
            end
        end

        return _AddWordPredictionDictionary(self, data, ...)
    end

    local rets = {_DoInit(self, ...)}

    TextEdit.AddWordPredictionDictionary = _AddWordPredictionDictionary

    return unpack(rets)
end
