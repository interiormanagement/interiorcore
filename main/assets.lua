local TheNet = GLOBAL.TheNet

PrefabFiles = {
    -- house prefabs
    "interior_handle",

    -- debug
    "debug_door",
    "interior_minimapentity_classified",
}

Assets = {
    -- house Assets
    --Asset("SHADER", "shaders/billboard.ksh"), -- rip
	Asset("SHADER", "shaders/interior.ksh"),
    Asset("SHADER", "shaders/rotating_billboard.ksh"),
    Asset("IMAGE", "levels/textures/interiors/floor_cityhall.tex"),
    Asset("IMAGE", "levels/textures/interiors/shop_wall_woodwall.tex"),
	Asset("IMAGE", "levels/textures/interiors/antcave_floor.tex"),
	Asset("IMAGE", "levels/textures/interiors/antcave_wall_rock.tex"),
    Asset("IMAGE", "levels/textures/interiors/batcave_floor.tex"),
    Asset("IMAGE", "levels/textures/interiors/batcave_wall_rock.tex"),
	Asset("IMAGE", "levels/textures/interiors/floor_marble_royal.tex"),
	Asset("IMAGE", "levels/textures/interiors/wall_royal_high.tex"),
    Asset("IMAGE", "levels/textures/interiors/sourceerror.tex"),
	Asset("IMAGE", "images/colour_cubes/pigshop_interior_cc.tex"),

    Asset("ATLAS", "levels/textures/map_interior/mini_antcave_floor.xml"),
    Asset("IMAGE", "levels/textures/map_interior/mini_antcave_floor.tex"),
    Asset("ATLAS", "levels/textures/map_interior/mini_floor_marble_royal.xml"),
    Asset("IMAGE", "levels/textures/map_interior/mini_floor_marble_royal.tex"),
    Asset("ATLAS", "levels/textures/map_interior/mini_ruins_slab.xml"),
    Asset("IMAGE", "levels/textures/map_interior/mini_ruins_slab.tex"),
    Asset("ATLAS", "levels/textures/map_interior/mini_vamp_cave_noise.xml"),
    Asset("IMAGE", "levels/textures/map_interior/mini_vamp_cave_noise.tex"),
    Asset("ATLAS", "levels/textures/map_interior/mini_sourceerror.xml"),
    Asset("IMAGE", "levels/textures/map_interior/mini_sourceerror.tex"),
    
	Asset("ATLAS", "levels/textures/map_interior/frame.xml"),
	Asset("IMAGE", "levels/textures/map_interior/frame.tex"),

	Asset("ATLAS", "levels/textures/map_interior/exit.xml"),
	Asset("IMAGE", "levels/textures/map_interior/exit.tex"),

	Asset("ATLAS", "levels/textures/map_interior/passage.xml"),
	Asset("IMAGE", "levels/textures/map_interior/passage.tex"),
	Asset("ATLAS", "levels/textures/map_interior/passage_blocked.xml"),
	Asset("IMAGE", "levels/textures/map_interior/passage_blocked.tex"),
	Asset("ATLAS", "levels/textures/map_interior/passage_unknown.xml"),
	Asset("IMAGE", "levels/textures/map_interior/passage_unknown.tex"),
}

-- TODO: These should be seperated from the minimap atlases  -Half
AddMinimapAtlas("levels/textures/map_interior/mini_antcave_floor.xml")
AddMinimapAtlas("levels/textures/map_interior/mini_floor_marble_royal.xml")
AddMinimapAtlas("levels/textures/map_interior/mini_ruins_slab.xml")
AddMinimapAtlas("levels/textures/map_interior/mini_vamp_cave_noise.xml")
AddMinimapAtlas("levels/textures/map_interior/mini_sourceerror.xml")
