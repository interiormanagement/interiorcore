local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local ACTIONS = {
    USEINTERIORDOOR = Action({}),
}

for name, ACTION in pairs(ACTIONS) do
    ACTION.id = name
    ACTION.str = STRINGS.ACTIONS[name] or "PL_ACTION"
    IAENV.AddAction(ACTION)
end


ACTIONS.USEINTERIORDOOR.fn = function(act)
	if act.target:HasTag("secret_room") or act.target:HasTag("predoor") then
		return false
	end

	if act.target.components.interiordoor and not act.target.components.interiordoor.disabled then
		act.target.components.interiordoor:Activate(act.doer)
		return true
	elseif act.target.components.interiordoor and act.target.components.interiordoor.disabled then
		return false, "LOCKED"
	end
end

-- SCENE        using an object in the world
-- USEITEM      using an inventory item on an object in the world
-- POINT        using an inventory item on a point in the world
-- EQUIPPED     using an equiped item on yourself or a target object in the world
-- INVENTORY    using an inventory item
local COMPONENT_ACTIONS =
{
    SCENE = { -- args: inst, doer, actions, right
        interiordoor = function(inst, doer, actions, right)
            if not inst:HasTag("predoor") then
                table.insert(actions, ACTIONS.USEINTERIORDOOR)
            end
        end
    },

    USEITEM = { -- args: inst, doer, target, actions, right
    },

    POINT = { -- args: inst, doer, pos, actions, right, target

    },

    EQUIPPED = { -- args: inst, doer, target, actions, right

    },

    INVENTORY = { -- args: inst, doer, actions, right

    },
    ISVALID = { -- args: inst, action, right

    },
}

for actiontype, actons in pairs(COMPONENT_ACTIONS) do
    for component, fn in pairs(actons) do
        IAENV.AddComponentAction(actiontype, component, fn)
    end
end

-- hack
local COMPONENT_ACTIONS = IAENV.Debug_Util.GetUpvalue(EntityScript.CollectActions, "COMPONENT_ACTIONS")
local SCENE = COMPONENT_ACTIONS.SCENE
local USEITEM = COMPONENT_ACTIONS.USEITEM
local POINT = COMPONENT_ACTIONS.POINT
local EQUIPPED = COMPONENT_ACTIONS.EQUIPPED
local INVENTORY = COMPONENT_ACTIONS.INVENTORY
