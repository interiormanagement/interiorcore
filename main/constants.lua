GLOBAL.setfenv(1, GLOBAL)

-- Maybe rename this to InteriorBillBoard because that makes more sense? -Half
ANIM_ORIENTATION.RotatingBillBoard = GetTableSize(ANIM_ORIENTATION) + 1

-- house constants
-- TODO: Put these in the dimension_defs file -Half

-- NOTES(JBK): These constants are from MiniMapRenderer ZOOM_CLAMP_MIN and ZOOM_CLAMP_MAX
MINIMAP = {
    ROOM_SPACING = 81,
    ICON_SCALE = .7,
    ZOOM_CLAMP_MAX = 20,
    ZOOM_CLAMP_MIN = 1,
}

INTERIOR_FLOOR = "levels/textures/interiors/floor_cityhall.tex"
INTERIOR_FLOOR_WOOD = "levels/textures/noise_woodfloor.tex"
INTERIOR_WALL = "levels/textures/interiors/shop_wall_woodwall.tex"

INTERIOR_FLOOR_ANT = "levels/textures/interiors/antcave_floor.tex"
INTERIOR_WALL_ANT = "levels/textures/interiors/antcave_wall_rock.tex"
INTERIOR_MINIMAP_ANT = "mini_antcave_floor.tex"

INTERIOR_FLOOR_PALACE = "levels/textures/interiors/floor_marble_royal.tex"
INTERIOR_WALL_PALACE = "levels/textures/interiors/wall_royal_high.tex"
INTERIOR_MINIMAP_PALACE = "mini_floor_marble_royal.tex"

INTERIOR_FLOOR_BATCAVE = "levels/textures/interiors/batcave_floor.tex"
INTERIOR_WALL_BATCAVE = "levels/textures/interiors/batcave_wall_rock.tex"
INTERIOR_MINIMAP_BATCAVE = "mini_vamp_cave_noise.tex"

INTERIOR_FLOOR_RUINS = "levels/textures/ground_ruins_slab.tex"
INTERIOR_WALL_RUINS = "levels/textures/ground_ruins_slab.tex"
INTERIOR_MINIMAP_RUINS = "mini_ruins_slab.tex"